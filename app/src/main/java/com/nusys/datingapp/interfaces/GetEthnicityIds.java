package com.nusys.datingapp.interfaces;

public interface GetEthnicityIds {

    public void getEthnicityId(String n);
}

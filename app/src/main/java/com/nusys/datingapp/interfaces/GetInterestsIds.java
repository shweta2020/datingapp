package com.nusys.datingapp.interfaces;

import java.util.ArrayList;

public interface GetInterestsIds {

    void getInterestsId(ArrayList<String> ids);
}

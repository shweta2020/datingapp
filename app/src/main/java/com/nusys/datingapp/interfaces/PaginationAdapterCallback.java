package com.nusys.datingapp.interfaces;

/**
 * Created by: Shweta Agarwal
 */

public interface PaginationAdapterCallback {

    void retryPageLoad();
}

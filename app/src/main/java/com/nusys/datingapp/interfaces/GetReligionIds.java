package com.nusys.datingapp.interfaces;

public interface GetReligionIds {

    public void getReligionId(String n);
}

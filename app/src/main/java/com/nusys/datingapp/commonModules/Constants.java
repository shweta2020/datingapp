package com.nusys.datingapp.commonModules;

/**
 * Created By: Shweta Agarwal
 * Created DAte:11-08-2020
 **/
public class Constants {

    public static final String BASE_URL = "http://103.205.64.158/~nsystechlg/datingapp/api/web_service/";
    public static final String CONTENT_TYPE = "application/json";

    public static final int GALLERY_REQUEST_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;

    /**
     * login api POST API
     * using retrofit
     * Created Date: 14-08-2020
     **/
    public static final String LOGIN = "login";
    /**
     * signup POST API
     * using volley
     * Created Date: 19-08-2020
     **/
    public static final String SIGNUP = "registration";
    /**
     * view interested in list GET API
     * using retrofit
     * Created Date: 19-08-2020
     **/
    public static final String INTERESTED_IN = "interest";

    /**
     * view country in list GET API
     * using retrofit
     * Created Date: 20-08-2020
     **/
    public static final String COUNTRY_LIST = "country";

    /**
     * view city in list POST API
     * using retrofit
     * Created Date: 20-08-2020
     **/
    public static final String CITY_LIST = "city";
    /**
     * view body type in list POST API
     * using retrofit
     * Created Date: 20-08-2020
     **/
    public static final String BODY_TYPE = "body_types";
    /**
     * view ethnicity in list POST API
     * using retrofit
     * Created Date: 21-08-2020
     **/
    public static final String ETHNICITY = "ethnicity";

    /**
     * view marital status list POST API
     * using retrofit
     * Created Date: 21-08-2020
     **/
    public static final String MARITAL_STATUS = "marital_status";
    /**
     * view religion in list POST API
     * using retrofit
     * Created Date: 21-08-2020
     **/
    public static final String RELIGION = "religion";
    /**
     * view children in list POST API
     * using retrofit
     * Created Date: 21-08-2020
     **/
    public static final String CHILDREN = "has_children";
    /**
     * view interests in list POST API
     * using retrofit
     * Created Date: 21-08-2020
     **/
    public static final String INTERESTS = "hobby";

    /**
     * upload pic POST API
     * using volley
     * Created Date: 24-08-2020
     **/
    public static final String UPLOAD_IMG = "upload_photo";

    /**
     * add country and city POST API
     * using volley
     * Created Date: 24-08-2020
     **/
    public static final String ADD_COUNTRY_CITY = "add_country_city";
    /**
     * add info1 POST API
     * using volley
     * Created Date: 24-08-2020
     **/
    public static final String ADD_INFO1 = "add_user_info_step1";
    /**
     * add info2 POST API
     * using volley
     * Created Date: 24-08-2020
     **/
    public static final String ADD_INFO2 = "add_user_info_step2";
    /**
     * add info3 POST API
     * using volley
     * Created Date: 25-08-2020
     **/
    public static final String ADD_INFO3 = "add_user_info_step3";
    /**
     * add info3 POST API
     * using volley
     * Created Date: 25-08-2020
     **/
    public static final String ADD_INFO4 = "user_profession";
    /**
     * meet POST API using paging
     * using volley
     * Created Date: 25-08-2020
     **/
    public static final String MEET = "meet";

    /**
     * activity list POST API using paging
     * using retrofit
     * Created Date: 31-08-2020
     **/
    public static final String ACTIVITY = "activity";

    /**
     * profile for individual user POST API
     * using volley
     * Created Date: 03-09-2020
     **/
    public static final String INDIVIDUAL_PROFILE = "single_user_detail";

    /**
     * change password POST API
     * using volley
     * Created Date: 03-09-2020
     **/
    public static final String CHANGE_PWD = "update_password";

    /**
     * forgot password POST API
     * using retrofit
     * Created Date: 03-09-2020
     **/
    public static final String FORGOT_PWD = "forgot_password";

    /**
     * deactivate acc POST API
     * using volley
     * Created Date: 03-09-2020
     **/
    public static final String DEACTIVATE_ACC = "deactivate_account";

    /**
     * message list POST API
     * using retrofit
     * Created Date: 04-09-2020
     **/
    public static final String MESSAGE_LIST = "message";

    /**
     * likes list POST API
     * using retrofit
     * Created Date: 04-09-2020
     **/
    public static final String LIKES_LIST = "who_likes_you";
    /**
     * online list POST API
     * using retrofit
     * Created Date: 04-09-2020
     **/
    public static final String ONLINE_LIST = "who_is_online";
    /**
     * who viewed me list POST API
     * using retrofit
     * Created Date: 04-09-2020
     **/
    public static final String VIEWED_LIST = "who_viewed_me";
    /**
     * fetch profile detail POST API
     * using retrofit
     * Created Date: 08-09-2020
     **/
    public static final String FETCH_PROFILE = "user_profile";

    /**
     * update profile POST API
     * using volley
     * Created Date: 08-09-2020
     **/
    public static final String UPDATE_PROFILE = "edit_profile";


    /**
     * fetch profession list POST API
     * using retrofit
     * Created Date: 08-09-2020
     **/
    public static final String PROFESSION_LIST = "profession";
    /**
     * fetch height list POST API
     * using retrofit
     * Created Date: 09-09-2020
     **/
    public static final String HEIGHT_LIST = "height";
    /**
     * fetch plans POST API
     * using retrofit
     * Created Date: 10-09-2020
     **/
    public static final String PLANS = "membership_plan";
    /**
     * fetch plans detail POST API
     * using retrofit
     * Created Date: 11-09-2020
     **/
    public static final String PLANS_DETAIL = "plan_details";
    /* cancel plan POST API
     * using volley
     * Created Date: 11-09-2020
     **/
    public static final String CANCEL_PLAN = "cancel_premium_membership";

    /* cancel plan POST API
     * using volley
     * Created Date: 11-09-2020
     **/
    public static final String MATCH = "match";
    /* cancel plan POST API
     * using retrofit
     * Created Date: 14-09-2020
     **/
    public static final String WITH_IN_LIST = "live_within";
    /**
     * update mail privacy POST API
     * using volley
     * Created Date: 14-09-2020
     **/
    public static final String MAIL_PRIVACY = "mail_privacy";
    /* pics list POST API
     * using retrofit
     * Created Date: 15-09-2020
     **/
    public static final String PICS_LIST = "fetch_photo";
    /* i liked list POST API
     * using retrofit
     * Created Date: 16-09-2020
     **/
    public static final String I_LIKED_LIST = "liked";
    /* delete image POST API
     * using volley
     * Created Date: 16-09-2020
     **/
    public static final String DELETE_PIC = "delete_image";
    /**
     * fetch profile detail POST API
     * using retrofit
     * Created Date: 08-09-2020
     **/
    public static final String FETCH_EDIT_VALUES = "fetch_profile";
}

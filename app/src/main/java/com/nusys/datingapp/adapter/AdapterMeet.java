package com.nusys.datingapp.adapter;

import android.content.Context;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.IndividualProfile;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.PaginationAdapterCallback;
import com.nusys.datingapp.models.HomeMeetModel;

import java.util.ArrayList;

import java.util.List;


public class AdapterMeet extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<HomeMeetModel> post_list;
    private Context context;
    private Context contextForFragmentImage;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;
    SharedPreference_main sharedPreference_main;


    public AdapterMeet(Context context) {

        this.mCallback = (PaginationAdapterCallback) context;
        post_list = new ArrayList<>();
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.adapter_meet, parent, false);
                viewHolder = new ItemVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        final HomeMeetModel result = post_list.get(position); // Movie

        switch (getItemViewType(position)) {

            case ITEM:

                //SARE SET VALUES
                final ItemVH holder1 = (ItemVH) holder;
                // HomeMeetModel profilePostM = question_list.get(position);
                holder1.nameAge.setText(result.getName() + "," + result.getAge());
                //contextForFragmentImage is used bec fragment not attached with image
                contextForFragmentImage = holder.itemView.getContext();
                Glide.with(contextForFragmentImage)
                        .load(result.getImage())
                        .placeholder(R.drawable.extrapic)
                        .into(holder1.imgIcon);
                holder1.imgIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, IndividualProfile.class);
                        i.putExtra("user_id", result.getId());
                        context.startActivity(i);

                    }
                });

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    //   loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    //  loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (null != post_list ? post_list.size() : 0);
        //  return post_list == null ? 0 : post_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == post_list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }


    public void add(HomeMeetModel r) {
        post_list.add(r);
        notifyItemInserted(post_list.size() - 1);
    }


    public void addAll(List<HomeMeetModel> moveResults) {
        for (HomeMeetModel result : moveResults) {
            add(result);
        }
    }


    private void remove(HomeMeetModel r) {
        int position = post_list.indexOf(r);
        if (position > -1) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new HomeMeetModel());
    }


    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = post_list.size() - 1;
        HomeMeetModel result = getItem(position);

        if (result != null) {
            post_list.remove(position);
            notifyItemRemoved(position);
        }
    }


    public HomeMeetModel getItem(int position) {
        return post_list.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(post_list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ItemVH extends RecyclerView.ViewHolder {
        private TextView nameAge;
        private ImageView imgIcon;


        public ItemVH(View itemView) {
            super(itemView);

            nameAge = itemView.findViewById(R.id.tv_name_age);
            imgIcon = itemView.findViewById(R.id.img_icon);


        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        // private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            // mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}

package com.nusys.datingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.models.LikesModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-09-2020
 * Updated Date:
 **/

public class AdapterLikes extends RecyclerView.Adapter<AdapterLikes.myholder> {

    Context context;
    List<LikesModel.Datum> data;
    private int selected_position = -1;


    public AdapterLikes(Context context, List<LikesModel.Datum> data) {
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public AdapterLikes.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_meet, parent, false);
        return new AdapterLikes.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterLikes.myholder holder, final int i) {

        holder.nameAge.setText(data.get(i).getName()+","+data.get(i).getAge());
        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.imgIcon);
        holder.cvLayout.setAlpha((float)0.3);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        private TextView nameAge;
        private ImageView imgIcon;
        private CardView cvLayout;

        public myholder(@NonNull View itemView) {
            super(itemView);
            nameAge = itemView.findViewById(R.id.tv_name_age);
            imgIcon = itemView.findViewById(R.id.img_icon);
            cvLayout = itemView.findViewById(R.id.cv_layout);


        }
    }
}
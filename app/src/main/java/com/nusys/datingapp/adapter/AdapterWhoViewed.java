package com.nusys.datingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.models.WhoViewedModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:20-08-2020
 * Updated Date: 21-08-2020(add visibility acc to selection)
 **/

public class AdapterWhoViewed extends RecyclerView.Adapter<AdapterWhoViewed.myholder> {

    Context context;
    List<WhoViewedModel.Datum> data;
    private int selected_position = -1;


    public AdapterWhoViewed(Context context, List<WhoViewedModel.Datum> data) {
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public AdapterWhoViewed.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_meet, parent, false);
        return new AdapterWhoViewed.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterWhoViewed.myholder holder, final int i) {

        holder.nameAge.setText(data.get(i).getName());
        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.imgIcon);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        private TextView nameAge;
        private ImageView imgIcon;

        public myholder(@NonNull View itemView) {
            super(itemView);
            nameAge = itemView.findViewById(R.id.tv_name_age);
            imgIcon = itemView.findViewById(R.id.img_icon);


        }
    }
}
package com.nusys.datingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.interfaces.GetInterestsIds;
import com.nusys.datingapp.models.InterestedInModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:20-08-2020
 * Updated Date: 21-08-2020(add visibility acc to selection)
 **/

public class AdapterInterests extends RecyclerView.Adapter<AdapterInterests.myholder> {

    Context context;
    List<InterestedInModel.Datum> data , selectedIn;
    private int selected_position = -1;
    GetInterestsIds getInterestsIds;
    boolean visibility_maintain = false;
    ArrayList<String> get_ID = new ArrayList<String>();
    //ArrayList<String> ;

    public AdapterInterests(Context context, List<InterestedInModel.Datum> data, GetInterestsIds getInterestsIds) {
        this.context = context;
        this.data = data;
        this.getInterestsIds = getInterestsIds;
        this.selectedIn = new ArrayList<>();
    }

    @NonNull
    @Override
    public AdapterInterests.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_interested_in, parent, false);
        return new AdapterInterests.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterInterests.myholder holder, final int i) {



        holder.tvHeading.setText(data.get(i).getName());
        holder.tvHeading.setPadding(40, 40, 40, 40);

        final InterestedInModel.Datum item = data.get(i);
        holder.tvHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedIn.contains(item)){
                    selectedIn.remove(item);

                    get_ID.remove(data.get(holder.getAdapterPosition()).getId());
                    getInterestsIds.getInterestsId(get_ID);
                    holder.tvHeading.setBackgroundResource(R.drawable.button_border);
                    holder.tvHeading.setTextColor(Color.parseColor("#aa1d21"));
                    holder.tvHeading.setPadding(40, 40, 40, 40);
                }else {
                    selectedIn.add(item);

                    get_ID.add(data.get(holder.getAdapterPosition()).getId());
                    getInterestsIds.getInterestsId(get_ID);

                    holder.tvHeading.setBackgroundResource(R.drawable.button_fill);
                    holder.tvHeading.setTextColor(Color.WHITE);
                    holder.tvHeading.setPadding(40, 40, 40, 40);
                }
            }
        });

        if (selectedIn.contains(item)){
            holder.tvHeading.setBackgroundResource(R.drawable.button_fill);
            holder.tvHeading.setTextColor(Color.WHITE);
            holder.tvHeading.setPadding(40, 40, 40, 40);
        }else {
            holder.tvHeading.setBackgroundResource(R.drawable.button_border);
            holder.tvHeading.setTextColor(Color.parseColor("#aa1d21"));
            holder.tvHeading.setPadding(40, 40, 40, 40);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);


        }
    }
}
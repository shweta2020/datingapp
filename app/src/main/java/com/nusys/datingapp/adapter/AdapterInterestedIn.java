package com.nusys.datingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.interfaces.GetInterestedInIds;
import com.nusys.datingapp.models.InterestedInModel;


import java.util.List;

import static androidx.core.content.res.ResourcesCompat.getColor;

/**
 * Created By: Shweta Agarwal
 * Created DAte:20-08-2020
 * Updated Date: 21-08-2020(add visibility acc to selection)
 **/

public class AdapterInterestedIn extends RecyclerView.Adapter<AdapterInterestedIn.myholder> {

    Context context;
    List<InterestedInModel.Datum> data;
    private int selected_position = -1;
    GetInterestedInIds getInterestedInIds;

    public AdapterInterestedIn(Context context, List<InterestedInModel.Datum> data, GetInterestedInIds getInterestedInIds) {
        this.context = context;
        this.data = data;
        this.getInterestedInIds = getInterestedInIds;
    }

    @NonNull
    @Override
    public AdapterInterestedIn.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_interested_in, parent, false);
        return new AdapterInterestedIn.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterInterestedIn.myholder holder, final int i) {

        holder.tvHeading.setText(data.get(i).getName());
        holder.tvHeading.setPadding(60, 60, 60, 60);

        if (data.get(holder.getAdapterPosition()).isSelected()) {
            holder.tvHeading.setBackgroundResource(R.drawable.button_fill);
            holder.tvHeading.setTextColor(Color.WHITE);
            holder.tvHeading.setPadding(40, 40, 40, 40);

        } else {
            holder.tvHeading.setBackgroundResource(R.drawable.button_border);
            holder.tvHeading.setTextColor(Color.parseColor("#aa1d21"));
            holder.tvHeading.setPadding(40, 40, 40, 40);
        }

        holder.tvHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected_position == holder.getAdapterPosition()) {
                    selected_position = -1;
                    data.get(holder.getAdapterPosition()).setSelected(false);
                    notifyDataSetChanged();
                    return;
                }
                selected_position = holder.getAdapterPosition();
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected(false);
                }
                data.get(holder.getAdapterPosition()).setSelected(true);
                getInterestedInIds.getInterestedId(data.get(holder.getAdapterPosition()).getId());
                //Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getId(), Toast.LENGTH_SHORT).show();
                /*Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getOptionId(), Toast.LENGTH_SHORT).show();
                Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getQuestionId(), Toast.LENGTH_SHORT).show();
*/
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);


        }
    }
}
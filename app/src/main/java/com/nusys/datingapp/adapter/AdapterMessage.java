package com.nusys.datingapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.Chat;
import com.nusys.datingapp.models.MessageModel;

import java.util.List;

import static com.nusys.datingapp.commonModules.Extension.getCapsSentences;

/**
 * Created By: Shweta Agarwal
 * Created DAte:31-08-2020
 * Updated Date:04-08-2020(apply api and parameters)
 **/

public class AdapterMessage extends RecyclerView.Adapter<AdapterMessage.myholder> {

    Context context;
    List<MessageModel.Datum> data;
    String capText;

    public AdapterMessage(Context context, List<MessageModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterMessage.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_message, parent, false);
        return new AdapterMessage.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMessage.myholder holder, final int i) {

        capText= getCapsSentences(data.get(i).getName());
        holder.tvName.setText(capText);
        holder.tvMsg.setText(data.get(i).getMessage());

        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.civImg);
        holder.llLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context, Chat.class);
                in.putExtra("msgID",data.get(i).getMessageId());
                context.startActivity(in);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvName, tvMsg;
        ImageView civImg;
        LinearLayout llLayout;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvMsg = itemView.findViewById(R.id.tv_time);
            civImg = itemView.findViewById(R.id.civ_image);
            llLayout = itemView.findViewById(R.id.ll_layout);

        }
    }
}
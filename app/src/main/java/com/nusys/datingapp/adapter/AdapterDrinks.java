package com.nusys.datingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.interfaces.GetDrinksIds;
import com.nusys.datingapp.models.WantChildrenModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:20-08-2020
 * Updated Date: 21-08-2020(add visibility acc to selection)
 **/

public class AdapterDrinks extends RecyclerView.Adapter<AdapterDrinks.myholder> {

    Context context;
    List<WantChildrenModel> data;
    private int selected_position = -1;
    GetDrinksIds getDrinksIds;

    public AdapterDrinks(Context context, List<WantChildrenModel> data, GetDrinksIds getDrinksIds) {
        this.context = context;
        this.data = data;
        this.getDrinksIds = getDrinksIds;
    }

    @NonNull
    @Override
    public AdapterDrinks.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_interested_in, parent, false);
        return new AdapterDrinks.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterDrinks.myholder holder, final int i) {

        holder.tvHeading.setText(data.get(i).getName());
        holder.tvHeading.setPadding(60, 60, 60, 60);

        if (data.get(holder.getAdapterPosition()).isSelected()) {
            holder.tvHeading.setBackgroundResource(R.drawable.button_fill);
            holder.tvHeading.setTextColor(Color.WHITE);
            holder.tvHeading.setPadding(40, 40, 40, 40);

        } else {
            holder.tvHeading.setBackgroundResource(R.drawable.button_border);
            holder.tvHeading.setTextColor(Color.parseColor("#aa1d21"));
            holder.tvHeading.setPadding(40, 40, 40, 40);
        }

        holder.tvHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected_position == holder.getAdapterPosition()) {
                    selected_position = -1;
                    data.get(holder.getAdapterPosition()).setSelected(false);
                    notifyDataSetChanged();
                    return;
                }
                selected_position = holder.getAdapterPosition();
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected(false);
                }
                data.get(holder.getAdapterPosition()).setSelected(true);
                getDrinksIds.getDrinksId(data.get(holder.getAdapterPosition()).getValueId());

                /*Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getOptionId(), Toast.LENGTH_SHORT).show();
                Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getQuestionId(), Toast.LENGTH_SHORT).show();
*/
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);


        }
    }
}
package com.nusys.datingapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.IndividualProfile;
import com.nusys.datingapp.interfaces.GetPicDeleteIds;
import com.nusys.datingapp.models.LikesModel;
import com.nusys.datingapp.models.PicsModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:15-09-2020
 * Updated Date:16-09-2020(add conditions and delete Id)
 **/

public class AdapterPics extends RecyclerView.Adapter<AdapterPics.myholder> {

    Context context;
    List<PicsModel.Datum> data;
    private int selected_position = -1;
    GetPicDeleteIds getPicDeleteIds;


    public AdapterPics(Context context, List<PicsModel.Datum> data, GetPicDeleteIds getPicDeleteIds) {
        this.context = context;
        this.data = data;
        this.getPicDeleteIds = getPicDeleteIds;

    }

    @NonNull
    @Override
    public AdapterPics.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_pic, parent, false);
        return new AdapterPics.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterPics.myholder holder, final int i) {

        // holder.nameAge.setText(data.get(i).getName()+","+data.get(i).getAge());
        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.imgIcon);
        if (data.get(i).getImageType().equals("2")) {
            if (data.get(i).getImageType().equals("1")) {
                holder.tvImageType.setText("Public Profile");
            } else {
                holder.tvImageType.setText("Public");
            }
        } else if (data.get(i).getImageType().equals("3")) {
            holder.tvImageType.setText("Private");
        }
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPicDeleteIds.getPicDeleteId(data.get(i).getId());

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {

        private ImageView imgIcon, ivDelete;
        private TextView tvSetProfile, tvImageType;

        public myholder(@NonNull View itemView) {
            super(itemView);

            imgIcon = itemView.findViewById(R.id.img_icon);
            ivDelete = itemView.findViewById(R.id.iv_delete);
            tvSetProfile = itemView.findViewById(R.id.tv_setProfile);
            tvImageType = itemView.findViewById(R.id.tv_imageType);


        }
    }
}
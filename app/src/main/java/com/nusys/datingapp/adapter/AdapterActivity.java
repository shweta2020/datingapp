package com.nusys.datingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.models.ActivityModel;


import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:31-08-2020
 * Updated Date: 03-09-2020(apply valid api)
 **/

public class AdapterActivity extends RecyclerView.Adapter<AdapterActivity.myholder> {

    Context context;
    List<ActivityModel.Datum> data;

    public AdapterActivity(Context context, List<ActivityModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterActivity.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_activity_list, parent, false);
        return new AdapterActivity.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterActivity.myholder holder, final int i) {

        holder.tvName.setText(data.get(i).getName());
        holder.tvTime.setText(data.get(i).getCreatedAt());

        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.civImg);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvName, tvTime;
        ImageView civImg;
        RelativeLayout rlD;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
            civImg = itemView.findViewById(R.id.civ_image);
            rlD = itemView.findViewById(R.id.rl_d);

        }
    }
}
package com.nusys.datingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetSmokesIds;
import com.nusys.datingapp.models.FetchProfileModel;
import com.nusys.datingapp.models.WantChildrenModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:20-08-2020
 * Updated Date: 21-08-2020(add visibility acc to selection)
 **/

public class AdapterSmokes extends RecyclerView.Adapter<AdapterSmokes.myholder> {

    Context context;
    List<WantChildrenModel> data;
    private int selected_position = -1;
    GetSmokesIds getSmokesIds;
    SharedPreference_main sharedPreference_main;

    public AdapterSmokes(Context context, List<WantChildrenModel> data, GetSmokesIds getSmokesIds) {
        this.context = context;
        this.data = data;
        this.getSmokesIds = getSmokesIds;
        sharedPreference_main=SharedPreference_main.getInstance(context);
    }

    @NonNull
    @Override
    public AdapterSmokes.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_interested_in, parent, false);
        return new AdapterSmokes.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterSmokes.myholder holder, final int i) {

        holder.tvHeading.setText(data.get(i).getName());
        holder.tvHeading.setPadding(60, 60, 60, 60);

        if (data.get(holder.getAdapterPosition()).isSelected()) {
            holder.tvHeading.setBackgroundResource(R.drawable.button_fill);
            holder.tvHeading.setTextColor(Color.WHITE);
            holder.tvHeading.setPadding(40, 40, 40, 40);

        } else {
            holder.tvHeading.setBackgroundResource(R.drawable.button_border);
            holder.tvHeading.setTextColor(Color.parseColor("#aa1d21"));
            holder.tvHeading.setPadding(40, 40, 40, 40);
        }

        holder.tvHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected_position == holder.getAdapterPosition()) {
                    selected_position = -1;
                    data.get(holder.getAdapterPosition()).setSelected(false);
                    notifyDataSetChanged();
                    return;
                }
                selected_position = holder.getAdapterPosition();
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected(false);
                }
                data.get(holder.getAdapterPosition()).setSelected(true);
                getSmokesIds.getSmokesId(data.get(holder.getAdapterPosition()).getValueId());

                /*Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getOptionId(), Toast.LENGTH_SHORT).show();
                Toast.makeText(context, ""+data.get(holder.getAdapterPosition()).getQuestionId(), Toast.LENGTH_SHORT).show();
*/
                notifyDataSetChanged();
            }
        });
    }
    private void user_profile() {

        HashMap<String, String> map = new HashMap<>();

        map.put("id", sharedPreference_main.getUserId());
        map.put("fetch_type", "smokes");

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<FetchProfileModel> call = serviceInterface.fetch_editValues(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<FetchProfileModel>() {
            @Override
            public void onResponse(Call<FetchProfileModel> call, retrofit2.Response<FetchProfileModel> response) {

                if (response.isSuccessful()) {

                    FetchProfileModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        //selectedId=bean.getData().getMaritalId();
                        /*etAddress2.setText(bean.getData().get(0).getAddress2());
                        Glide.with(getApplicationContext())
                                .load(bean.getData().get(0).getProfilePic())
                                .placeholder(R.drawable.user_icon)
                                .into(imgProfile);*/


                    } else {

                        //Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FetchProfileModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);


        }
    }
}
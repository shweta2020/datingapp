package com.nusys.datingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.models.OnlineModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-08-2020
 * Updated Date:
 **/

public class AdapterOnline extends RecyclerView.Adapter<AdapterOnline.myholder> {

    Context context;
    List<OnlineModel.Datum> data;
    String capText;

    public AdapterOnline(Context context, List<OnlineModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterOnline.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_online, parent, false);
        return new AdapterOnline.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterOnline.myholder holder, final int i) {

        Glide.with(context)
                .load(data.get(i).getImage())
                .placeholder(R.drawable.extrapic)
                .into(holder.civImg);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {

        ImageView civImg;


        public myholder(@NonNull View itemView) {
            super(itemView);

            civImg = itemView.findViewById(R.id.img_icon);


        }
    }
}
package com.nusys.datingapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created Date:03-09-2020
 * Updated Date:
 **/
public class ProfileModel {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("age")
        @Expose
        private String age;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("seeking")
        @Expose
        private String seeking;
        @SerializedName("main_img")
        @Expose
        private String mainImg;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("country_name")
        @Expose
        private String countryName;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("interested_in")
        @Expose
        private String interestedIn;
        @SerializedName("body_type")
        @Expose
        private String bodyType;
        @SerializedName("marital_status")
        @Expose
        private String maritalStatus;
        @SerializedName("ethnicity")
        @Expose
        private String ethnicity;
        @SerializedName("religion")
        @Expose
        private String religion;
        @SerializedName("has_children_name")
        @Expose
        private String hasChildrenName;
        @SerializedName("want_child")
        @Expose
        private String wantChild;
        @SerializedName("smokeing")
        @Expose
        private String smokeing;
        @SerializedName("drinking")
        @Expose
        private String drinking;
        @SerializedName("profession")
        @Expose
        private String profession;
        @SerializedName("about")
        @Expose
        private String about;
        @SerializedName("hoby_name")
        @Expose
        private String hobyName;
        @SerializedName("email")
        @Expose
        private String email;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSeeking() {
            return seeking;
        }

        public void setSeeking(String seeking) {
            this.seeking = seeking;
        }

        public String getMainImg() {
            return mainImg;
        }

        public void setMainImg(String mainImg) {
            this.mainImg = mainImg;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getInterestedIn() {
            return interestedIn;
        }

        public void setInterestedIn(String interestedIn) {
            this.interestedIn = interestedIn;
        }

        public String getBodyType() {
            return bodyType;
        }

        public void setBodyType(String bodyType) {
            this.bodyType = bodyType;
        }

        public String getMaritalStatus() {
            return maritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }

        public String getEthnicity() {
            return ethnicity;
        }

        public void setEthnicity(String ethnicity) {
            this.ethnicity = ethnicity;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getHasChildrenName() {
            return hasChildrenName;
        }

        public void setHasChildrenName(String hasChildrenName) {
            this.hasChildrenName = hasChildrenName;
        }

        public String getWantChild() {
            return wantChild;
        }

        public void setWantChild(String wantChild) {
            this.wantChild = wantChild;
        }

        public String getSmokeing() {
            return smokeing;
        }

        public void setSmokeing(String smokeing) {
            this.smokeing = smokeing;
        }

        public String getDrinking() {
            return drinking;
        }

        public void setDrinking(String drinking) {
            this.drinking = drinking;
        }

        public String getProfession() {
            return profession;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getHobyName() {
            return hobyName;
        }

        public void setHobyName(String hobyName) {
            this.hobyName = hobyName;
        }
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}

package com.nusys.datingapp.models;
/**
 * Created By: Shweta Agarwal
 * Created DAte:24-08-2020
 * Updated Date:
 **/
public class WantChildrenModel {
    private String valueId;
    private String name;
    public WantChildrenModel(String valueId, String name) {
        this.valueId = valueId;
        this.name = name;

    }
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

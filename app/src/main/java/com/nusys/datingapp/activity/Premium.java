package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.PlanModel;
import com.nusys.datingapp.models.ProfileModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:10-09-2020(apply api)
 **/
public class Premium extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    TextView tvToolbarHead;
    LinearLayout llBackActivity;
    TextView tvMonthTop, tvPriceTop, tvDurationTop, tvMonthMiddle, tvPriceMiddle;
    TextView tvDurationMiddle, tvMonthLow, tvPriceLow, tvDurationLow;
    LinearLayout llPremium, llGold, llBasic;
    String idPremium, idGold, idBasic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        initialization();
        action();
    }

    public void initialization() {
        context = Premium.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks


        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvMonthTop = findViewById(R.id.tv_month_top);
        tvPriceTop = findViewById(R.id.tv_price_top);
        tvDurationTop = findViewById(R.id.tv_duration_top);
        tvMonthMiddle = findViewById(R.id.tv_month_middle);
        tvPriceMiddle = findViewById(R.id.tv_price_middle);
        tvDurationMiddle = findViewById(R.id.tv_duration_middle);
        tvMonthLow = findViewById(R.id.tv_month_low);
        tvPriceLow = findViewById(R.id.tv_price_low);
        tvDurationLow = findViewById(R.id.tv_duration_low);
        llPremium = findViewById(R.id.ll_premium);
        llGold = findViewById(R.id.ll_gold);
        llBasic = findViewById(R.id.ll_basic);

    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Premium");
        /* finish activity when click on bake arrow
         * Created Date: 01-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /* calling method for fetching plans
         * Created Date: 10-09-2020
         */
        plan();


        /* intent to plan detail when click on llPremium
         * Created Date: 11-09-2020
         */

        llPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,PlanDetail.class);
                i.putExtra("plan_id",idPremium);
                startActivity(i);
            }
        });/* intent to plan detail when click on llGold
         * Created Date: 11-09-2020
         */

        llGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // llGold.setBackground(getDrawable(R.drawable.button_square_red_border));
                Intent i=new Intent(context,PlanDetail.class);
                i.putExtra("plan_id",idGold);
                startActivity(i);
            }
        });
        /* intent to plan detail when click on llBasic
         * Created Date: 11-09-2020
         */

        llBasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,PlanDetail.class);
                i.putExtra("plan_id",idBasic);
                startActivity(i);
            }
        });
    }

    /*  retrofit method for fetching plan
     *  Created Date: 10-09-2020
     */
    private void plan() {
        if (NetworkUtil.isConnected(context)) {

            //   pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("id", sharedPreference_main.getUserId());
            map.put("gender", sharedPreference_main.getgender());

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<PlanModel> call = serviceInterface.fetch_plan(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<PlanModel>() {
                @Override
                public void onResponse(Call<PlanModel> call, retrofit2.Response<PlanModel> response) {

                    if (response.isSuccessful()) {

                        PlanModel bean = response.body();
                        if (bean.getStatus()) {

                            for (int i = 0; i < bean.getData().size(); i++) {
                                if (bean.getData().get(i).getId().equals("1")) {
                                    tvMonthLow.setText(bean.getData().get(i).getDuration() + " months");
                                    tvPriceLow.setText(bean.getData().get(i).getPrice() + " \u20B9");
                                    idBasic = bean.getData().get(i).getId();

                                } else if (bean.getData().get(i).getId().equals("3")) {
                                    tvMonthMiddle.setText(bean.getData().get(i).getDuration() + " months");
                                    tvPriceMiddle.setText(bean.getData().get(i).getPrice() + " \u20B9");
                                    idGold = bean.getData().get(i).getId();

                                } else if (bean.getData().get(i).getId().equals("4")) {
                                    tvMonthTop.setText(bean.getData().get(i).getDuration() + " months");
                                    tvPriceTop.setText(bean.getData().get(i).getPrice() + " \u20B9");
                                    idPremium = bean.getData().get(i).getId();

                                }
                            }


                            // pd.dismiss();

                        } else {
                            // pd.dismiss();
                            // errorLayout.setVisibility(View.VISIBLE);
                            // llMain.setVisibility(View.GONE);
                            // imgPickFrom.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        //  pd.dismiss();
                        // errorLayout.setVisibility(View.VISIBLE);
                        // llMain.setVisibility(View.GONE);
                        // imgPickFrom.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PlanModel> call, Throwable t) {
                    // pd.dismiss();
                    // errorLayout.setVisibility(View.VISIBLE);
                    // llMain.setVisibility(View.GONE);
                    //  imgPickFrom.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }
}
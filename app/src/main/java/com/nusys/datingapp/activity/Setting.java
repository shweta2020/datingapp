package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:
 **/
public class Setting extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    TextView tvToolbarHead, tvChangePwd, tvDeactivateAcc, tvFilterMail, tvMailSetting;
    LinearLayout llBackActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initialization();
        action();
    }

    public void initialization() {
        context = Setting.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks


        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tvChangePwd = findViewById(R.id.tv_change_pwd);
        tvDeactivateAcc = findViewById(R.id.tv_deactivate_acc);
        tvFilterMail = findViewById(R.id.tv_filter_mail);
        tvMailSetting = findViewById(R.id.tv_mail_setting);
        llBackActivity = findViewById(R.id.ll_back_activity);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Setting");
        /**
         * intent in change password when click on tvChangePwd
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ChangePassword.class));
            }
        });

        /**
         * intent in Deactivate Account when click on tvDeactivateAcc
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvDeactivateAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, DeactivateAccount.class));
            }
        });
        /**
         * intent in Deactivate Account when click on tvDeactivateAcc
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvFilterMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, FilterMail.class));
            }
        });
        /**
         * intent in Deactivate Account when click on tvDeactivateAcc
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvMailSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MailSetting.class));
            }
        });

        /* finish activity when click on bake arrow
         * Created Date: 01-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
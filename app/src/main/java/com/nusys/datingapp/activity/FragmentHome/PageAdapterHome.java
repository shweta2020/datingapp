package com.nusys.datingapp.activity.FragmentHome;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nusys.datingapp.activity.FragmentLike.ILiked;
import com.nusys.datingapp.activity.FragmentLike.MyLikes;

public class PageAdapterHome extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PageAdapterHome(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MyLikes tab1 = new MyLikes();
                return tab1;
            case 1:
                ILiked tab2 = new ILiked();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

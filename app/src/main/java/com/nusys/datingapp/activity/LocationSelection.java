package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.CityModel;
import com.nusys.datingapp.models.CountryModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.ADD_COUNTRY_CITY;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.UPDATE_PROFILE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:11-08-2020
 * Updated Date:
 **/
public class LocationSelection extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks

    Spinner spCountry, spCity;
    TextView tvSaveShow, tvSave;

    private ArrayList<String> spinCountry = new ArrayList<String>();
    private ArrayList<String> spinCountry_id = new ArrayList<String>();

    private ArrayList<String> spinCity = new ArrayList<String>();
    private ArrayList<String> spinCity_id = new ArrayList<String>();
    String spCountryID, spCityID;
    String selectPage = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
        initialization();
        action();
    }

    public void initialization() {
        context = LocationSelection.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        spCity = findViewById(R.id.sp_city);
        spCountry = findViewById(R.id.sp_country);
        tvSaveShow = findViewById(R.id.tv_save_show);
        tvSave = findViewById(R.id.tv_save);

        spinCountry.add("Select Country");
        spinCountry_id.add("-1");

        //location=true ==>SignupWithPhone.java & location=false ==>EditProfile.java
        selectPage = getIntent().getStringExtra("location");
    }

    public void action() {

        /*  calling view_country_list method
         *  Created Date: 20-08-2020
         */
        view_country_list();
        /*  action for taking spinner country value
         *  Created Date: 20-08-2020
         *  Updated Date:
         */

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCountryID = spinCountry_id.get(position);
                view_city_list(spCountryID);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*  action for taking spinner city value
         *  Created Date:20-08-2020
         *  Updated Date:
         */
        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCityID = spinCity_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*redirect to Pic selection page when click on tvSaveShow
         * Created Date: 28-07-2020
         * Updated Date: 24-08-2020(call add_country_city method)
         */
        tvSaveShow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(context, ProfilePicUpload.class);
                startActivity(i);*/
                if (selectPage.equals("true")) {
                    add_country_city();

                }else {
                    update_profile();
                }



            }

        });
    }

    /*  method for fetching country list
     *  Created Date: 20-08-2020
     */

    private void view_country_list() {
        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CountryModel> call = serviceInterface.country_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, retrofit2.Response<CountryModel> response) {
                if (response.isSuccessful()) {
                    CountryModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String countryName = bean.getData().get(i).getName();
                            String countryID = bean.getData().get(i).getId();

                            spinCountry.add(countryName);
                            spinCountry_id.add(countryID);

                        }

                    } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spCountry.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCountry));

                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * method for fetching city list
     * Created Date: 20-08-2020
     **/

    private void view_city_list(String id_country) {
        spinCity.clear();
        spinCity_id.clear();
        spinCity.add("Select City");
        spinCity_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id_country);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CityModel> call = serviceInterface.city_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                if (response.isSuccessful()) {
                    CityModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String cityName = bean.getData().get(i).getCityName();
                            String cityId = bean.getData().get(i).getId();


                            spinCity.add(cityName);
                            spinCity_id.add(cityId);

                        }
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    } else {
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    }

                } else {
                    spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                }
            }


            @Override
            public void onFailure(Call<CityModel> call, Throwable t) {
                spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

            }
        });


    }


    /*  method for add location
     *  Created Date: 24-08-2020
     * Updated Date: 03-09-2020(add if condition for redirect to signup or editprofile screen)
     * Updated Date: 04-09-2020(in if condition add selectPage variable)
     */

    private void add_country_city() {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("country_id", spCountryID);
            jsonBody.put("city_id", spCityID);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_COUNTRY_CITY, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                                    Intent i = new Intent(context, ProfilePicUpload.class);
                                    startActivity(i);



                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    /*  volley method for update location
     *  Created Date: 08-09-2020
     *  Updated Date:
     */
    private void update_profile() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("update_type", "location");
            jsonBody.put("country_id", spCountryID);
            jsonBody.put("city_id", spCityID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + UPDATE_PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                Intent i = new Intent(context, EditProfile.class);
                                startActivity(i);
                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

}
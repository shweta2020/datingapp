package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.ProfileModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.UPDATE_PROFILE;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:27-08-2020
 * Updated Date:
 **/

public class EditProfile extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvLocationSelection, tvHeightEdit, tvMaritalEdit, tvBodyTypeEdit, tvChildrenEdit;
    TextView tvInterestedInEdit, tvSmokesEdit, tvDrinkEdit, tvEmail, tvAge, tvGender, tvAboutEdit;
    TextView tvWantChildEdit, tvEthnicityEdit, tvReligionEdit, tvInterestsEdit, tvProfessionEdit;
    ImageView imgProfile, imgPickFrom;
    EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initialization();
        action();
    }

    public void initialization() {
        context = EditProfile.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);

        tvLocationSelection = findViewById(R.id.tv_location_selection);
        tvHeightEdit = findViewById(R.id.tv_height_edit);
        tvMaritalEdit = findViewById(R.id.tv_marital_edit);
        tvBodyTypeEdit = findViewById(R.id.tv_bodyType_edit);
        tvChildrenEdit = findViewById(R.id.tv_children_edit);
        tvInterestedInEdit = findViewById(R.id.tv_interested_in_edit);
        tvSmokesEdit = findViewById(R.id.tv_smokes_edit);
        tvDrinkEdit = findViewById(R.id.tv_drink_edit);
        tvWantChildEdit = findViewById(R.id.tv_want_child_edit);
        tvEthnicityEdit = findViewById(R.id.tv_ethnicity_edit);
        tvReligionEdit = findViewById(R.id.tv_religion_edit);
        tvInterestsEdit = findViewById(R.id.tv_interests_edit);
        imgProfile = findViewById(R.id.img_Profile);
        tvEmail = findViewById(R.id.tv_email);
        etName = findViewById(R.id.et_name);
        tvGender = findViewById(R.id.tv_gender);
        tvAge = findViewById(R.id.tv_age);
        tvProfessionEdit = findViewById(R.id.tv_profession_edit);
        tvAboutEdit = findViewById(R.id.tv_about_edit);
        imgPickFrom = findViewById(R.id.img_pickFrom);

    }

    public void action() {
        /* intent to location selection screen when click on tvLocationSelection
         * Created Date: 01-08-2020
         */

        tvLocationSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, LocationSelection.class);
                i.putExtra("location", "false");
                startActivity(i);
            }
        });

        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvHeightEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "height");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvMaritalEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "marital_status");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvBodyTypeEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "body_type");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvChildrenEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "children");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvInterestedInEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "interested");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvSmokesEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "smokes");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvDrinkEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "drinks");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvWantChildEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "want_children");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvEthnicityEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "ethnicity");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvReligionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "religion");
                startActivity(i);
            }
        });
        /* intent to height selection screen when click on tvHeightEdit
         * Created Date: 01-08-2020
         */

        tvInterestsEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "hobby");
                startActivity(i);
            }
        });
        /* intent to profession screen when click on tvHeightEdit
         * Created Date: 08-08-2020
         */

        tvProfessionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "profession");
                startActivity(i);
            }
        });
        /* intent to about screen when click on tvAboutEdit
         * Created Date: 09-08-2020
         */

        tvAboutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "about_me");
                startActivity(i);
            }
        });
        /* intent to dob screen when click on tvAge
         * Created Date: 09-08-2020
         */

        tvAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditAll.class);
                i.putExtra("updateType", "dob");
                startActivity(i);
            }
        });

        /**
         * calling method user_profile()
         * Created DAte: 08-09-2020
         * Updated Date:
         **/
        user_profile();
        /* intent to upload image screen when click on imgPickFrom
         * Created Date: 15-09-2020
         */

        imgPickFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, UploadImage.class);
               // i.putExtra("updateType", "dob");
                startActivity(i);
            }
        });
    }

    /*  retrofit method for fetching user profile
     *  Created Date: 08-09-2020
     */
    private void user_profile() {
        if (NetworkUtil.isConnected(context)) {

            //   pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("id", sharedPreference_main.getUserId());

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ProfileModel> call = serviceInterface.fetch_profile(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<ProfileModel>() {
                @Override
                public void onResponse(Call<ProfileModel> call, retrofit2.Response<ProfileModel> response) {

                    if (response.isSuccessful()) {

                        ProfileModel bean = response.body();
                        if (bean.getStatus()) {


                            etName.setText(bean.getData().get(0).getName());
                            tvAge.setText(bean.getData().get(0).getAge());
                            tvGender.setText(bean.getData().get(0).getGender());
                            tvEmail.setText(bean.getData().get(0).getEmail());
                            tvBodyTypeEdit.setText(bean.getData().get(0).getBodyType());
                            tvChildrenEdit.setText(bean.getData().get(0).getHasChildrenName());
                            tvDrinkEdit.setText(bean.getData().get(0).getDrinking());
                            tvEthnicityEdit.setText(bean.getData().get(0).getEthnicity());
                            tvHeightEdit.setText(bean.getData().get(0).getHeight());
                            tvInterestedInEdit.setText(bean.getData().get(0).getInterestedIn());
                            tvLocationSelection.setText(bean.getData().get(0).getCountryName() + "," + bean.getData().get(0).getCityName());
                            tvInterestsEdit.setText(bean.getData().get(0).getHobyName());
                            tvMaritalEdit.setText(bean.getData().get(0).getMaritalStatus());
                            tvReligionEdit.setText(bean.getData().get(0).getReligion());
                            tvSmokesEdit.setText(bean.getData().get(0).getSmokeing());
                            tvWantChildEdit.setText(bean.getData().get(0).getWantChild());
                            tvProfessionEdit.setText(bean.getData().get(0).getProfession());
                            tvAboutEdit.setText(bean.getData().get(0).getAbout());
                            Glide.with(getApplicationContext())
                                    .load(bean.getData().get(0).getMainImg())
                                    .placeholder(R.drawable.user_icon)
                                    .into(imgProfile);
                            // pd.dismiss();

                        } else {
                            // pd.dismiss();
                            // errorLayout.setVisibility(View.VISIBLE);
                            // llMain.setVisibility(View.GONE);
                            // imgPickFrom.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        //  pd.dismiss();
                        // errorLayout.setVisibility(View.VISIBLE);
                        // llMain.setVisibility(View.GONE);
                        // imgPickFrom.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfileModel> call, Throwable t) {
                    // pd.dismiss();
                    // errorLayout.setVisibility(View.VISIBLE);
                    // llMain.setVisibility(View.GONE);
                    //  imgPickFrom.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    /*  volley method for update_profile
     *  Created Date: 09-09-2020
     *  Updated Date:
     */
    private void update_profile() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("update_type", "display_name");
            jsonBody.put("display_name", etName.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + UPDATE_PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                // Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                              /*  Intent i = new Intent(context, EditProfile.class);
                                startActivity(i);*/
                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
    /*  on backpressed intent to home
     *  Created Date: 09-09-2020
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        update_profile();
        Intent i = new Intent(context, Home.class);
        i.putExtra("id", "1");
        startActivity(i);
        finish();
    }
}
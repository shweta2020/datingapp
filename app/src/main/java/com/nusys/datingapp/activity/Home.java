package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nusys.datingapp.activity.FragmentLike.LikeHome;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.FragmentMessageView.MessageHome;
import com.nusys.datingapp.adapter.AdapterMeet;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.PaginationScrollListener;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.fragments.CardstackMatch;
import com.nusys.datingapp.fragments.MoreFragment;
import com.nusys.datingapp.interfaces.PaginationAdapterCallback;
import com.nusys.datingapp.models.HomeMeetModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static android.view.View.*;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.MEET;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:13-08-2020
 * Updated Date: 25-08-2020(add pagination and api)
 **/
public class Home extends AppCompatActivity implements PaginationAdapterCallback, SwipeRefreshLayout.OnRefreshListener {
    Context context;
    SharedPreference_main sharedPreference_main;

    FloatingActionButton fabMatch;
    TextView tvMeetHeader, tvMatchHeader;
    LinearLayout llSetting, llLikes, llHome, llMsg;

    RecyclerView rv_post_list;
    AdapterMeet myAdapter;

    LinearLayoutManager linearLayoutManager;
    private static int TOTAL_PAGES;
    //  ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = 1;
    //  private ProgressDialog pd;
    FrameLayout mainFrame, frame_container;
    Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initialization();
        action();
    }

    public void initialization() {
        context = Home.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks
        fabMatch = findViewById(R.id.fab_match);
        tvMeetHeader = findViewById(R.id.tv_meet_header);
        tvMatchHeader = findViewById(R.id.tv_match_header);

        llSetting = findViewById(R.id.ll_setting);
        llLikes = findViewById(R.id.ll_likes);
        llHome = findViewById(R.id.ll_home);
        llMsg = findViewById(R.id.ll_msg);


        // Toast.makeText(this, "token"+sharedPreference_main.getToken(), Toast.LENGTH_SHORT).show();
        //  progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        rv_post_list = findViewById(R.id.rv_meetList);
        mainFrame = findViewById(R.id.mainFrameHome);

       /* pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);*/
        // txtError = findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
        frame_container = findViewById(R.id.frame_container_home);
      /*  GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        rv_post_list.setLayoutManager(manager);*/


    }

    public void action() {
        /*conditions for visibility of frames for fragments and add security fragment when we click on profile security
         * Created Date: 27-02-2020
         */
        /*if (getIntent().getStringExtra("id") != null) {
            setTitle("Security");
            frame_container.setVisibility(VISIBLE);
            mainFrame.setVisibility(GONE);

            MoreFragment moreFragment = new MoreFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.frame_container_home, moreFragment);
            transaction.addToBackStack("Security");
            transaction.commit();
        } else {
            frame_container.setVisibility(GONE);
            mainFrame.setVisibility(VISIBLE);
        }*/
        /*intent to cardStackMatch when click on floating action button
         * Created Date: 01-07-2020
         */

        fabMatch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new CardstackMatch();
                loadFragment(fragment);
             //   startActivity(new Intent(context, CardstackMatch.class));
            }
        });

        tvMatchHeader.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        tvMatchHeader.setTextColor(getResources().getColor(R.color.white));

        /* intent to cardStackMatch when click on tvMatchHeader
         * Created Date: 26-08-2020
         */

        tvMatchHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new CardstackMatch();
                loadFragment(fragment);
               // startActivity(new Intent(context, CardstackMatch.class));
            }
        });

        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Home.class));
            }
        });
        llLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new LikeHome();
                loadFragment(fragment);
                //  startActivity(new Intent(context, LikesMe.class));
            }
        });

        llMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MessageHome();
                loadFragment(fragment);
                //startActivity(new Intent(context, Message.class));
            }
        });
        llSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MoreFragment();
                loadFragment(fragment);
                // startActivity(new Intent(context, MoreFragment.class));
            }
        });

        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        rv_post_list.setLayoutManager(linearLayoutManager);

        rv_post_list.setItemAnimator(new DefaultItemAnimator());

        frame_container.setVisibility(View.GONE);
        mainFrame.setVisibility(View.VISIBLE);

        myAdapter = new AdapterMeet(Home.this);
        rv_post_list.setAdapter(myAdapter);

        rv_post_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //loadFirstPage();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);


    }
    /**
     * loadFragment method for loading fragments
     * Created DAte:04-09-2020
     **/
    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            mainFrame.setVisibility(View.GONE);
            frame_container.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container_home, fragment);
            transaction.addToBackStack("CalenderWebview");
            getSupportFragmentManager().popBackStackImmediate();
            transaction.commit();

        }
    }

    public void onRefresh() {
        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.

        //  progressBar.setVisibility(View.VISIBLE);
        isLastPage = false;

        swipeRefreshLayout.setRefreshing(false);
        loadFirstPage();
    }

    private void loadFirstPage() {
        //changed
        currentPage = 1;
        //relative.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
        //   frame_container.setVisibility(View.GONE);
        // To ensure list is visible when retry button in error view is clicked
        if (NetworkUtil.isConnected(context)) {
            hideErrorView();
            // pd.show();

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();
            try {

             /*   jsonBody.put("id", sharedPreference_main.getUserId());
                jsonBody.put("age_from", sharedPreference_main.getUserId());
                jsonBody.put("age_to", sharedPreference_main.getUserId());
                jsonBody.put("city_name", currentPage);*/

                jsonBody.put("id", 9);
                jsonBody.put("age_from", 18);
                jsonBody.put("age_to", 30);
                jsonBody.put("city_name", "delhi");
                jsonBody.put("page_no", currentPage);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + MEET, jsonBody,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response", response.toString());
                            try {

                                Boolean status = response.getBoolean("status");

                                if (status) {
                                    myAdapter.clear();
                                    myAdapter.notifyDataSetChanged();

                                    final List<HomeMeetModel> ab = new ArrayList<>();
                                    //JSONObject jsonObj = response.getJSONObject("data");
                                    JSONArray jsonArray = response.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        HomeMeetModel entity = new HomeMeetModel();
                                        //  entity.setData(response.getString("namezz"));
                                        entity.setName(jsonObject.getString("name"));
                                        entity.setId(jsonObject.getString("id"));

                                        entity.setAge(jsonObject.getString("age"));
                                        entity.setImage(jsonObject.getString("image"));
                                        entity.setLike(jsonObject.getInt("like"));
                                        ab.add(entity);
                                    }

                                    if (ab != null) {

                                        TOTAL_PAGES = ab.size();
                                        Log.e("totalPage", String.valueOf(ab.size()));
                                        myAdapter.addAll(ab);
//                                    rv_post_list.setAdapter(myAdapter);

                                    } else {

                                        Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();
                                    }
                                    //   progressBar.setVisibility(View.GONE);
                                    if (currentPage <= TOTAL_PAGES) myAdapter.addLoadingFooter();
                                    else isLastPage = true;
                                    //  pd.dismiss();
                                } else {
                                    //   pd.dismiss();
                                    ShowErrorView();
                                    Toast.makeText(Home.this, "Something is wrong try again later", Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                            //resultTextView.setText("String Response : "+ response.toString());
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //pd.dismiss();
                    ShowErrorView();
                    //  resultTextView.setText("Error getting response");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", sharedPreference_main.getToken());
                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadFirstPage();
//        onRefresh();
    }

    private void loadNextPage() {
        Log.d("page", "loadNextPage: " + currentPage);
        Log.e("total", String.valueOf(TOTAL_PAGES));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                final JSONObject jsonBody = new JSONObject();
                try {
                  /*  jsonBody.put("id", sharedPreference_main.getUserId());
                    jsonBody.put("age_from", sharedPreference_main.getUserId());
                    jsonBody.put("age_to", sharedPreference_main.getUserId());
                    jsonBody.put("city_name", currentPage); */

                    jsonBody.put("id", 9);
                    jsonBody.put("age_from", 18);
                    jsonBody.put("age_to", 30);
                    jsonBody.put("city_name", "delhi");
                    jsonBody.put("page_no", String.valueOf(currentPage));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + MEET, jsonBody,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("response", response.toString());
                                try {

                                    myAdapter.removeLoadingFooter();
                                    isLoading = false;

                                    Boolean status = response.getBoolean("status");

                                    if (status) {

                                        final List<HomeMeetModel> ab = new ArrayList<>();

                                        JSONArray jsonArray = response.getJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            HomeMeetModel entity = new HomeMeetModel();
                                            //  entity.setData(response.getString("namezz"));
                                            entity.setName(jsonObject.getString("name"));

                                            entity.setAge(jsonObject.getString("age"));
                                            entity.setImage(jsonObject.getString("image"));
                                            entity.setLike(jsonObject.getInt("like"));

                                            ab.add(entity);
                                        }


                                        if (ab != null) {
                                            //TOTAL_PAGES = ab.size();
                                            myAdapter.addAll(ab);
                                            // myAdapter.removeLoadingFooter();
                                            if (currentPage != TOTAL_PAGES)
                                                myAdapter.addLoadingFooter();

                                            else isLastPage = true;

                                        } else {
                                            isLastPage = true;

                                        }

                                    } else {
                                        isLastPage = true;

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    isLastPage = true;
                                }

                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLastPage = true;
                        Log.e("error", error.getMessage());

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", sharedPreference_main.getToken());
                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }
        }, 2000);

    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            //     progressBar.setVisibility(View.GONE);

            //txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            //  progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void ShowErrorView() {
        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            //   progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        return;

    }
}
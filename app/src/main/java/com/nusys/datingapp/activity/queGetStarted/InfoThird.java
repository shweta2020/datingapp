package com.nusys.datingapp.activity.queGetStarted;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterChildren;
import com.nusys.datingapp.adapter.AdapterDrinks;
import com.nusys.datingapp.adapter.AdapterInterests;
import com.nusys.datingapp.adapter.AdapterMaritalStatus;
import com.nusys.datingapp.adapter.AdapterReligion;
import com.nusys.datingapp.adapter.AdapterSmokes;
import com.nusys.datingapp.adapter.AdapterWantChildren;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetDrinksIds;
import com.nusys.datingapp.interfaces.GetInterestsIds;
import com.nusys.datingapp.interfaces.GetSmokesIds;
import com.nusys.datingapp.models.InterestedInModel;
import com.nusys.datingapp.models.WantChildrenModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.ADD_INFO2;
import static com.nusys.datingapp.commonModules.Constants.ADD_INFO3;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:19-08-2020
 * Updated Date:
 **/
public class InfoThird extends AppCompatActivity implements GetSmokesIds, GetDrinksIds, GetInterestsIds {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvSkip, tvSave;

    TextView tvSmokes, tvDrinks, tvInterests;
    LinearLayout llSmokes, llDrinks, llInterests;
    RecyclerView rvSmokesList, rvDrinksList, rvInterestsList;
    boolean visibility_maintain = false;

    AdapterDrinks adapterDrinks;
    AdapterSmokes adapterSmokes;
    AdapterInterests adapterInterests;
    String smokesId,drinksId, hobbyId;
    ArrayList<String> ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_third);
        initialization();
        action();

    }

    public void initialization() {
        context = InfoThird.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvSkip = findViewById(R.id.tv_skip);
        tvSave = findViewById(R.id.tv_save);

        tvSmokes = findViewById(R.id.tv_smokes);
        llSmokes = findViewById(R.id.ll_smokes);
        rvSmokesList = findViewById(R.id.rv_smokes_List);
        tvDrinks = findViewById(R.id.tv_drinks);
        llDrinks = findViewById(R.id.ll_drinks);
        rvDrinksList = findViewById(R.id.rv_drinks_List);
        tvInterests = findViewById(R.id.tv_interests);
        llInterests = findViewById(R.id.ll_interests);
        rvInterestsList = findViewById(R.id.rv_interests_list);

    }

    public void action() {

        /*skip showing marital status and goes to next page
         * Created Date: 19-08-2020
         */
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, InfoForth.class);
                startActivity(i);
            }
        });
        /*redirect to another page
         * Created Date: 19-08-2020
         */
        tvSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                add_info3();
                /*Intent i = new Intent(context, InfoForth.class);
                startActivity(i);*/
            }
        });

        /**
         * call fetch_marital_status method
         * Created Date: 21-08-2020
         **/
        fetch_smokes();

        /**
         * maintain visibility for religion and call fetch_religion method
         * Created Date: 21-08-2020
         **/
        tvDrinks.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_drinks();
                    llDrinks.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llDrinks.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });

        /**
         * maintain visibility for Children and call fetch_children method
         * Created Date: 21-08-2020
         **/
        tvInterests.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_interests();
                    llInterests.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llInterests.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });


    }

    /*  retrofit method for fetching smokes option in list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */
    private void fetch_smokes() {

        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0", "Any"));
        list.add(new WantChildrenModel("1", "No"));
        list.add(new WantChildrenModel("2", "Occasionally"));
        list.add(new WantChildrenModel("3", "Often"));


        adapterSmokes = new AdapterSmokes(context, (list),InfoThird.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvSmokesList.setLayoutManager(layoutManager);
                        rvSmokesList.setAdapter(adapterSmokes);

    }

    /*  retrofit method for fetching religion list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */
    private void fetch_drinks() {
        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0", "Any"));
        list.add(new WantChildrenModel("1", "No"));
        list.add(new WantChildrenModel("2", "Occasionally"));
        list.add(new WantChildrenModel("3", "Often"));


        adapterDrinks = new AdapterDrinks(context, (list),InfoThird.this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvDrinksList.setLayoutManager(layoutManager);
        rvDrinksList.setAdapter(adapterDrinks);


    }

    /*  retrofit method for fetching children list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(add valid api name)
     */

    private void fetch_interests() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_interests(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterInterests = new AdapterInterests(context, (bean.getData()),InfoThird.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvInterestsList.setLayoutManager(layoutManager);
                        rvInterestsList.setAdapter(adapterInterests);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  method for add information
     *  Created Date: 25-08-2020
     *  Updated Date:
     */

    private void add_info3() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {


            //  jsonBody.put("id", "18");
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("smokes", smokesId);
            jsonBody.put("drinks", drinksId);
            jsonBody.put("hobby_id", hobbyId);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_INFO3, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                                Intent i = new Intent(context, InfoForth.class);
                                startActivity(i);


                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void getDrinksId(String n) {
        drinksId=n;
    }

    @Override
    public void getInterestsId(ArrayList<String> ids) {
        this.ids = ids;
        Log.e("ids", ids.toString());
        String P_id = "";
        StringBuilder pd = new StringBuilder();
        for (Object mPI : ids) {
            pd.append(P_id).append(mPI);
            P_id = ",";
        }
        hobbyId = pd.toString();
       // Toast.makeText(context, ""+hobbyId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSmokesId(String n) {

        smokesId=n;
    }
}
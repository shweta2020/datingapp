package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterActivity;
import com.nusys.datingapp.commonModules.DialogProgress;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.ActivityModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:31-08-2020
 * Updated Date:03-09-2020(apply api)
 **/
public class Activity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvToolbarHead;
    LinearLayout llBackActivity;

    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    RecyclerView recyclerView_group_view;
    AdapterActivity Adapter;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);
        initialization();
        action();
    }

    public void initialization() {
        context = Activity.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);

        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);

        refreshLayout = findViewById(R.id.refresh_layout);
        mainProgress = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);

        recyclerView_group_view = findViewById(R.id.rv_group_view);

    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 01-09-2020
         */
        tvToolbarHead.setText("Activity Log");
        /* finish activity when click on bake arrow
         * Created Date: 01-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);

        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        /**
         * calling method of activity_list()
         * Created DAte: 03-09-2020
         * Updated Date:
         **/
        activity_list();

    }
    /*  retrofit method for fetching activity list
     *  Created Date: 03-09-2020
     */
    private void activity_list() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();
            map.put("id", sharedPreference_main.getUserId());

            //   if (NetworkUtils.isConnected(getActivity())) {
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ActivityModel> call = serviceInterface.activity_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<ActivityModel>() {


                @Override
                public void onResponse(Call<ActivityModel> call, retrofit2.Response<ActivityModel> response) {

                    if (response.isSuccessful()) {

                        ActivityModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus()) {
                            ActivityModel view = response.body();
                            errorLayout.setVisibility(View.GONE);
                            recyclerView_group_view.setVisibility(View.VISIBLE);

                            Adapter = new AdapterActivity(context, bean.getData());
                            recyclerView_group_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            recyclerView_group_view.setItemAnimator(new DefaultItemAnimator());
                            recyclerView_group_view.setAdapter(Adapter);
                            Log.e("Group_response", view.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            recyclerView_group_view.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_group_view.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ActivityModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_group_view.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                activity_list();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }

}
package com.nusys.datingapp.activity.FragmentMessageView;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterMessage;
import com.nusys.datingapp.adapter.AdapterOnline;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.MessageModel;
import com.nusys.datingapp.models.OnlineModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:27-08-2020
 * Updated Date: 04-09-2020(apply api for message list)
 * Updated Date: 04-09-2020(add header)
 * Updated Date: 04-08-2020(change activity to fragment)
 **/
public class Message extends Fragment {
    SharedPreference_main sharedPreference_main;

    RecyclerView recyclerView_online;
    AdapterOnline adapterOnline;

    RecyclerView recyclerView_message;
    AdapterMessage adapterMessage;

    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_message, container, false);
        initialization(view);
        action();
        return view;
    }
    public void initialization(View view) {
        // context = Message.this;
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        //Hooks
        recyclerView_online = view.findViewById(R.id.rv_online);

        errorLayout = view.findViewById(R.id.error_layout);
        errorTxtCause = view.findViewById(R.id.error_txt_cause);
        errorBtnRetry = view.findViewById(R.id.error_btn_retry);
        recyclerView_message = view.findViewById(R.id.rv_message);
    }

    public void action() {

        /**
         * calling method online_list()
         * Created DAte:04-09-2020
         * Updated Date:
         **/
        online_list();
        /**
         * calling retrofit method message_list()
         * Created DAte:04-09-2020
         * Updated Date:
         **/
        message_list();
    }

    /*  retrofit method for fetching online list
     *  Created Date: 04-09-2020
     */
    private void online_list() {
        if (NetworkUtil.isConnected(getContext())) {


            HashMap<String, String> map = new HashMap<>();
            map.put("id", sharedPreference_main.getUserId());

            //   if (NetworkUtils.isConnected(getActivity())) {
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<OnlineModel> call = serviceInterface.online_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<OnlineModel>() {
                @Override
                public void onResponse(Call<OnlineModel> call, retrofit2.Response<OnlineModel> response) {

                    if (response.isSuccessful()) {

                        OnlineModel bean = response.body();


                        if (bean.getStatus()) {
                            OnlineModel view = response.body();

                            adapterOnline = new AdapterOnline(getContext(), bean.getData());
                            recyclerView_online.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            recyclerView_online.setItemAnimator(new DefaultItemAnimator());
                            recyclerView_online.setAdapter(adapterOnline);
                            Log.e("Group_response", view.toString());
                            //pd.dismiss();
                        } else {

                            Toast.makeText(getContext(), bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getContext(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OnlineModel> call, Throwable t) {

                    Log.e("error", t.getMessage());
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(getContext(), new Dialog(getContext()));
        }
    }

    /*  retrofit method for fetching message list
     *  Created Date: 04-09-2020
     */
    private void message_list() {
        if (NetworkUtil.isConnected(getContext())) {

            // pd.show();
            HashMap<String, String> map = new HashMap<>();
            map.put("id", sharedPreference_main.getUserId());


            //   if (NetworkUtils.isConnected(getActivity())) {
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<MessageModel> call = serviceInterface.message_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<MessageModel>() {


                @Override
                public void onResponse(Call<MessageModel> call, retrofit2.Response<MessageModel> response) {

                    if (response.isSuccessful()) {

                        MessageModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus()) {
                            MessageModel view = response.body();
                            errorLayout.setVisibility(View.GONE);
                            recyclerView_message.setVisibility(View.VISIBLE);

                            adapterMessage = new AdapterMessage(getContext(), bean.getData());
                            recyclerView_message.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            recyclerView_message.setItemAnimator(new DefaultItemAnimator());
                            recyclerView_message.setAdapter(adapterMessage);
                            Log.e("Group_response", view.toString());
                            //  pd.dismiss();
                        } else {
                            // pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            recyclerView_message.setVisibility(View.GONE);
                            Toast.makeText(getContext(), bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        // pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        recyclerView_message.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    //   pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    recyclerView_message.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(getContext(), new Dialog(getContext()));
        }
    }
}
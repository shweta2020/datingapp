package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:
 **/
public class MailSetting extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvToolbarHead, tvChangePwd;
    LinearLayout llBackActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_setting);
        initialization();
        action();
    }

    public void initialization() {
        context = MailSetting.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tvChangePwd = findViewById(R.id.tv_change_pwd);
        llBackActivity = findViewById(R.id.ll_back_activity);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Mail Setting");

        /* finish activity when click on bake arrow
         * Created Date: 28-08-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /**
         * intent in change password when click on tvChangePwd
         * Created DAte:28-08-2020
         * Updated Date:
         **/
       /* tvChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ChangePassword.class));
            }
        });*/
    }
}
package com.nusys.datingapp.activity.FragmentLike;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.Premium;
import com.nusys.datingapp.adapter.AdapterLikes;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.LikesModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:26-08-2020
 * Updated Date: 04-08-2020(change activity to fragment)
 **/
public class MyLikes extends Fragment {
    SharedPreference_main sharedPreference_main;
    //Hooks
    Button btLikePremium;
    TextView tvText;
    RecyclerView rvMyLikes;
    AdapterLikes adapterLikes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_likes, container, false);
        initialization(view);
        action();
        return view;
    }

    public void initialization(View view) {

        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        //Hooks
        tvText = view.findViewById(R.id.tv_text);
        btLikePremium = view.findViewById(R.id.bt_like_premium);
        rvMyLikes = view.findViewById(R.id.rv_likeList);

    }

    public void action() {
        /* set text with color in tvText
         * Created Date: 31-08-2020
         */
        String first = "If you want to see who likes you. ";
        String next = "<font color='#aa1d21'>Please</font>";
        String nextToNext = " select your premium.";
        tvText.setText(Html.fromHtml(first + next + nextToNext));
        /* intent to premium screen when click on btLikePremium
         * Created Date: 01-09-2020
         */
        btLikePremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Premium.class));
            }
        });
        /**
         * calling retrofit method myLikes()
         * Created DAte:04-09-2020
         * Updated Date:
         **/
        myLikes();
    }

    /**
     * retrofit method for fetching likes list
     * Created Date: 04-08-2020
     **/

    private void myLikes() {

        HashMap<String, String> map = new HashMap<>();
        map.put("id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<LikesModel> call = serviceInterface.likes_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<LikesModel>() {
            @Override
            public void onResponse(Call<LikesModel> call, Response<LikesModel> response) {
                if (response.isSuccessful()) {
                    LikesModel bean = response.body();
                    if (bean.getStatus().equals(true)) {
                        adapterLikes = new AdapterLikes(getContext(), bean.getData());
                        rvMyLikes.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                        rvMyLikes.setItemAnimator(new DefaultItemAnimator());
                        rvMyLikes.setAdapter(adapterLikes);
                       /* adapterMyMatch = new AdapterLikes(context, (bean.getData()));
                        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        //layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
                      //  rvMyLikes.setHasFixedSize(true);
                        rvMyLikes.setLayoutManager(layoutManager);
                        rvMyLikes.setAdapter(adapterMyMatch);*/
                        Log.e("Group_response", bean.toString());

                    } else {
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<LikesModel> call, Throwable t) {

            }
        });


    }
}
package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.queGetStarted.InfoFirst;

/**
 * Created By: Shweta Agarwal
 * Created DAte:11-08-2020
 * Updated Date:
 **/
public class ReadyToMeet extends AppCompatActivity {
    Context context;
    //hooks
    TextView tvSkip, tvStart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready_to_meet);
        initialization();
        action();
    }

    public void initialization() {
        context = ReadyToMeet.this;
        //Hooks
        tvSkip = findViewById(R.id.tv_skip);
        tvStart = findViewById(R.id.tv_start);
    }

    public void action() {

        /*skip Ready to meet and goes to next page
         * Created Date: 29-07-2020
         */
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, Home.class);
                startActivity(i);
            }
        });
        /*redirect to another page and upload pic
         * Created Date: 28-07-2020
         */
        tvStart.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, InfoFirst.class);
                startActivity(i);
            }
        });
    }

}
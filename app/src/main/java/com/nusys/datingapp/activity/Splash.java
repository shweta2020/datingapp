package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.FragmentHome.HomeHome;
import com.nusys.datingapp.commonModules.Extension;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created By: Shweta Agarwal
 * Created DAte:10-08-2020
 * Updated Date:
 **/
public class Splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 6000;
    SharedPreference_main sharedPreference_main;
    Dialog dialog;
    //hooks
    ImageView slogan;
    ImageView ivLogo;
    //Animations
    Animation middleAnimation, bottomAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialization();
        action();
    }

    public void initialization() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        dialog = new Dialog(this);
        //Hooks
        ivLogo = findViewById(R.id.iv_logo);
        slogan = findViewById(R.id.tagLine);
    }

    public void action() {
        //splash on full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Animation Calls
        bottomAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        middleAnimation = AnimationUtils.loadAnimation(this, R.anim.middle_animation);

        //MoreFragment Animations to the elements of Splash
        ivLogo.setAnimation(middleAnimation);
        slogan.setAnimation(bottomAnimation);

        /**
         * Splash Screen Code to call new Activity after some time and  date validation for apk
         * Created DAte:10-08-2020
         * Updated Date:
         **/
        Date currentdate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String str2 = "30/10/2020";
        try {
            Date date2 = formatter.parse(str2);

            if (currentdate.after(date2)) {
                Extension.showErrorDialog(this, dialog);

            } else {
                if (NetworkUtil.isConnected(this)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            if (sharedPreference_main.getIs_LoggedIn()) {
                                startActivity(new Intent(Splash.this, Home.class));
                                finish();
                            } else {
                                Intent intent = new Intent(Splash.this, SelectionForSignupLogin.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    Extension.showErrorDialog(this, dialog);

                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.SIGNUP;
import static com.nusys.datingapp.commonModules.Extension.openCalender;

/**
 * Created By: Shweta Agarwal
 * Created DAte:11-08-2020
 * Updated Date:
 **/
public class SignupWithPhone extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvSubmit;
    ImageView ivMaleOn, ivMaleOff, ivFemaleOn, ivFemaleOff;
    ImageView ivManOn, ivManOff, ivWomanOn, ivWomanOff, ivBothOn, ivBothOff;
    EditText etEmail, etName, etPassword;
    TextView tvDob;
    //constants
    private int mYear, mMonth, mDay;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    int seeking = 0, gender = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_with_phone);
        initialization();
        action();
    }

    public void initialization() {
        context = SignupWithPhone.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvSubmit = findViewById(R.id.tv_submit);
        ivMaleOn = findViewById(R.id.iv_male_on);
        ivMaleOff = findViewById(R.id.iv_male_off);
        ivFemaleOn = findViewById(R.id.iv_female_on);
        ivFemaleOff = findViewById(R.id.iv_female_off);
        ivManOn = findViewById(R.id.iv_man_on);
        ivManOff = findViewById(R.id.iv_man_off);
        ivWomanOn = findViewById(R.id.iv_woman_on);
        ivWomanOff = findViewById(R.id.iv_woman_off);
        ivBothOn = findViewById(R.id.iv_both_on);
        ivBothOff = findViewById(R.id.iv_both_off);

        etEmail = findViewById(R.id.et_email);
        etName = findViewById(R.id.et_name);
        etPassword = findViewById(R.id.et_password);

        tvDob = findViewById(R.id.tv_dob);

    }

    public void action() {
        /* apply visibility when click on seeking and gender button
         * Created Date: 19-08-2020
         * Updated Date: 20-08-2020(add value for gender and seeker)
         */
        ivMaleOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMaleOff.setVisibility(View.VISIBLE);
                ivMaleOn.setVisibility(View.GONE);
                ivFemaleOff.setVisibility(View.GONE);
                ivFemaleOn.setVisibility(View.VISIBLE);
                gender = 2;

            }
        });
        ivMaleOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMaleOn.setVisibility(View.VISIBLE);
                ivMaleOff.setVisibility(View.GONE);
                ivFemaleOff.setVisibility(View.VISIBLE);
                ivFemaleOn.setVisibility(View.GONE);
                gender = 1;

            }
        });
        ivFemaleOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMaleOn.setVisibility(View.GONE);
                ivMaleOff.setVisibility(View.VISIBLE);
                ivFemaleOff.setVisibility(View.GONE);
                ivFemaleOn.setVisibility(View.VISIBLE);
                gender = 2;

            }
        });
        ivFemaleOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMaleOn.setVisibility(View.VISIBLE);
                ivMaleOff.setVisibility(View.GONE);
                ivFemaleOff.setVisibility(View.VISIBLE);
                ivFemaleOn.setVisibility(View.GONE);
                gender = 1;

            }
        });


        ivManOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.VISIBLE);
                ivManOn.setVisibility(View.GONE);
                ivWomanOff.setVisibility(View.GONE);
                ivWomanOn.setVisibility(View.VISIBLE);
                ivBothOff.setVisibility(View.VISIBLE);
                ivBothOn.setVisibility(View.GONE);
                seeking = 2;

            }
        });

        ivManOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.GONE);
                ivManOn.setVisibility(View.VISIBLE);
                ivWomanOff.setVisibility(View.VISIBLE);
                ivWomanOn.setVisibility(View.GONE);
                ivBothOff.setVisibility(View.VISIBLE);
                ivBothOn.setVisibility(View.GONE);
                seeking = 1;

            }
        });

        ivWomanOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.VISIBLE);
                ivManOn.setVisibility(View.GONE);
                ivWomanOff.setVisibility(View.VISIBLE);
                ivWomanOn.setVisibility(View.GONE);
                ivBothOff.setVisibility(View.GONE);
                ivBothOn.setVisibility(View.VISIBLE);
                seeking = 3;

            }
        });

        ivWomanOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.VISIBLE);
                ivManOn.setVisibility(View.GONE);
                ivWomanOff.setVisibility(View.GONE);
                ivWomanOn.setVisibility(View.VISIBLE);
                ivBothOff.setVisibility(View.VISIBLE);
                ivBothOn.setVisibility(View.GONE);
                seeking = 2;

            }
        });

        ivBothOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.GONE);
                ivManOn.setVisibility(View.VISIBLE);
                ivWomanOff.setVisibility(View.VISIBLE);
                ivWomanOn.setVisibility(View.GONE);
                ivBothOff.setVisibility(View.VISIBLE);
                ivBothOn.setVisibility(View.GONE);
                seeking = 1;

            }
        });

        ivBothOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivManOff.setVisibility(View.VISIBLE);
                ivManOn.setVisibility(View.GONE);
                ivWomanOff.setVisibility(View.VISIBLE);
                ivWomanOn.setVisibility(View.GONE);
                ivBothOff.setVisibility(View.GONE);
                ivBothOn.setVisibility(View.VISIBLE);
                seeking = 3;

            }
        });

        /* calling date picker when click on dob
         *  Created Date: 19-08-2020
         */

        tvDob.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                openCalender(context, mYear, mMonth, mDay, tvDob);

            }

        });

        /*redirect to select location page when click on tvSignup
         * Created Date: 27-07-2020
         * Updated Date: 20-08-2020(calling signup Api)
         * Updated Date: 20-08-2020(apply validation )
         */
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("field can't be empty");

                } else if (!etEmail.getText().toString().matches(emailPattern)) {

                    Toast.makeText(context, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {

                    etPassword.setError("field can't be empty");

                } else if (TextUtils.isEmpty(tvDob.getText().toString())) {

                    tvDob.setError("field can't be empty");

                } else if ((TextUtils.isEmpty(etName.getText().toString()))) {

                    etName.setError("field can't be empty");

                } else {
                    do_signup();
                }
            }
        });
    }

    /*  volley method for register user
     *  Created Date: 20-08-2020
     */
    private void do_signup() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("first_name", etName.getText().toString());
            jsonBody.put("gender", gender);
            jsonBody.put("seeking", seeking);
            jsonBody.put("email", etEmail.getText().toString());
            jsonBody.put("password", etPassword.getText().toString());
            jsonBody.put("dob", tvDob.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + SIGNUP, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");

                            if (status == true) {
                                JSONObject jsonObject=response.getJSONObject("data");
                                sharedPreference_main.setIs_LoggedIn(true);
                                sharedPreference_main.setUserId(jsonObject.getString("id"));
                                sharedPreference_main.setToken(response.getString("token"));
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                Intent i = new Intent(context, LocationSelection.class);
                                i.putExtra("location","true");
                                startActivity(i);

                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);

    }
}
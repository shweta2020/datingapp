package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.NetworkUtil;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.PlanDetailModel;
import com.nusys.datingapp.models.PlanModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CANCEL_PLAN;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.DEACTIVATE_ACC;
import static com.nusys.datingapp.commonModules.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:11-09-2020
 * Updated Date:
 **/
public class PlanDetail extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;

    //hooks
    TextView tvToolbarHead;
    LinearLayout llBackActivity;
    TextView tvPlanName,tvDuration, tvPrice, tvAmount, tvCancelPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_detail);
        initialization();
        action();
    }

    public void initialization() {
        context = PlanDetail.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tvPlanName = findViewById(R.id.tv_plan_name);
        tvDuration = findViewById(R.id.tv_duration);
        tvPrice = findViewById(R.id.tv_price);
        tvAmount = findViewById(R.id.tv_amount);
        tvCancelPlan = findViewById(R.id.tv_cancel_plan);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 11-09-2020
         */
        tvToolbarHead.setText("Plan Detail");

        /* finish activity when click on bake arrow
         * Created Date: 11-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* calling method for fetching plans detail
         * Created Date: 11-09-2020
         */
        plan_detail();
        /* calling method for cancel plans
         * Created Date: 11-09-2020
         */
        tvCancelPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_plan();
            }
        });
    }
    /*  retrofit method for fetching plan
     *  Created Date: 11-09-2020
     */
    private void plan_detail() {
        if (NetworkUtil.isConnected(context)) {

            //   pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("id", sharedPreference_main.getUserId());
            map.put("plan_id", getIntent().getStringExtra("plan_id"));

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<PlanDetailModel> call = serviceInterface.plan_detail(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<PlanDetailModel>() {
                @Override
                public void onResponse(Call<PlanDetailModel> call, retrofit2.Response<PlanDetailModel> response) {

                    if (response.isSuccessful()) {

                        PlanDetailModel bean = response.body();
                        if (bean.getStatus()) {

                            tvPlanName.setText(bean.getData().getPlanName());
                            tvDuration.setText(bean.getData().getDuration() + " months");
                            tvPrice.setText(bean.getData().getPrice() + " \u20B9");
                            tvAmount.setText(bean.getData().getPrice() + " \u20B9");

                            // pd.dismiss();

                        } else {
                            // pd.dismiss();
                            // errorLayout.setVisibility(View.VISIBLE);
                            // llMain.setVisibility(View.GONE);
                            // imgPickFrom.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        //  pd.dismiss();
                        // errorLayout.setVisibility(View.VISIBLE);
                        // llMain.setVisibility(View.GONE);
                        // imgPickFrom.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PlanDetailModel> call, Throwable t) {
                    // pd.dismiss();
                    // errorLayout.setVisibility(View.VISIBLE);
                    // llMain.setVisibility(View.GONE);
                    //  imgPickFrom.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }
    /*  volley method for deactivate acc
     *  Created Date: 03-09-2020
     */
    private void cancel_plan() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters


            jsonBody.put("id", sharedPreference_main.getUserId());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+CANCEL_PLAN, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {
                                sharedPreference_main.removePreference();
                                sharedPreference_main.setIs_LoggedIn(false);

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
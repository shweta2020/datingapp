package com.nusys.datingapp.activity.queGetStarted;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterChildren;
import com.nusys.datingapp.adapter.AdapterMaritalStatus;
import com.nusys.datingapp.adapter.AdapterReligion;
import com.nusys.datingapp.adapter.AdapterWantChildren;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetChildrenIds;
import com.nusys.datingapp.interfaces.GetMaritalStatusIds;
import com.nusys.datingapp.interfaces.GetReligionIds;
import com.nusys.datingapp.interfaces.GetWantChildrenIds;
import com.nusys.datingapp.models.AllProfileFetchModel;
import com.nusys.datingapp.models.InterestedInModel;
import com.nusys.datingapp.models.WantChildrenModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.datingapp.commonModules.Constants.ADD_INFO1;
import static com.nusys.datingapp.commonModules.Constants.ADD_INFO2;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:19-08-2020
 * Updated Date:21-08-2020(apply apis)
 **/
public class InfoSecond extends AppCompatActivity implements GetMaritalStatusIds, GetReligionIds, GetChildrenIds, GetWantChildrenIds {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvSkip, tvSave;
    TextView tvMaritalStatus, tvReligion, tvChildren, tvWantChildren;
    LinearLayout llReligion, llChildren, llWantChildren;
    boolean visibility_maintain = false;

    RecyclerView rvMaritalStatusList, rvReligionList, rvChildrenList, rvWantChildrenList;
    AdapterMaritalStatus adapterMaritalStatus;
    AdapterReligion adapterReligion;
    AdapterWantChildren adapterWantChildren;
    AdapterChildren adapterChildren;
    String maritalStatusId,wantChildrenId, religionId, childrenId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_second);
        initialization();
        action();

    }

    public void initialization() {
        context = InfoSecond.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvSkip = findViewById(R.id.tv_skip);
        tvSave = findViewById(R.id.tv_save);

        tvMaritalStatus = findViewById(R.id.tv_marital_status);
        tvReligion = findViewById(R.id.tv_religion);
        tvChildren = findViewById(R.id.tv_children);
        tvWantChildren = findViewById(R.id.tv_want_children);


        llReligion = findViewById(R.id.ll_religion);
        llChildren = findViewById(R.id.ll_children);
        llWantChildren = findViewById(R.id.ll_want_children);


        rvMaritalStatusList = findViewById(R.id.rv_marital_status_List);
        rvReligionList = findViewById(R.id.rv_religion_List);
        rvChildrenList = findViewById(R.id.rv_children_list);
        rvWantChildrenList = findViewById(R.id.rv_want_children_list);


    }

    public void action() {

        /*skip showing marital status and goes to next page
         * Created Date: 19-08-2020
         */
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, InfoThird.class);
                startActivity(i);
            }
        });
        /*redirect to another page
         * Created Date: 19-08-2020
         */
        tvSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
               /* Intent i = new Intent(context, InfoThird.class);
                startActivity(i);*/
                add_info2();

            }
        });

        /**
         * call fetch_marital_status method
         * Created Date: 21-08-2020
         **/
        fetch_marital_status();

        /**
         * maintain visibility for religion and call fetch_religion method
         * Created Date: 21-08-2020
         **/
        tvReligion.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_religion();
                    llReligion.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llReligion.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });

        /**
         * maintain visibility for Children and call fetch_children method
         * Created Date: 21-08-2020
         **/
        tvChildren.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_children();
                    llChildren.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llChildren.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });

        /**
         * maintain visibility for want children and call fetch_want_children method
         * Created Date: 21-08-2020
         **/
        tvWantChildren.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_want_children();
                    llWantChildren.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llWantChildren.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });


    }

    /*  retrofit method for fetching marital status in list
     *  Created Date: 21-08-2020
     */
    private void fetch_marital_status() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_marital_status(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterMaritalStatus = new AdapterMaritalStatus(context, (bean.getData()), InfoSecond.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvMaritalStatusList.setLayoutManager(layoutManager);
                        rvMaritalStatusList.setAdapter(adapterMaritalStatus);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching religion list
     *  Created Date: 21-08-2020
     */
    private void fetch_religion() {
        HashMap<String, String> map = new HashMap<>();

        map.put("id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<AllProfileFetchModel> call = serviceInterface.fetch_religion(sharedPreference_main.getToken(), CONTENT_TYPE,map);
        call.enqueue(new Callback<AllProfileFetchModel>() {


            @Override
            public void onResponse(Call<AllProfileFetchModel> call, retrofit2.Response<AllProfileFetchModel> response) {

                if (response.isSuccessful()) {

                    AllProfileFetchModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterReligion = new AdapterReligion(context, (bean.getData()),InfoSecond.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvReligionList.setLayoutManager(layoutManager);
                        rvReligionList.setAdapter(adapterReligion);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllProfileFetchModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching children list
     *  Created Date: 21-08-2020
     */

    private void fetch_children() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_children(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterChildren = new AdapterChildren(context, (bean.getData()), InfoSecond.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvChildrenList.setLayoutManager(layoutManager);
                        rvChildrenList.setAdapter(adapterChildren);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching want children list
     *  Created Date: 21-08-2020
     *  Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */

    private void fetch_want_children() {

        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0","Any"));
        list.add(new WantChildrenModel("1","No"));
        list.add(new WantChildrenModel("2","Yes"));
        list.add(new WantChildrenModel("3","Undecided/Open"));

        adapterWantChildren = new AdapterWantChildren(context, (list), InfoSecond.this);
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
            layoutManager.setFlexDirection(FlexDirection.ROW);
            layoutManager.setJustifyContent(JustifyContent.FLEX_START);
            rvWantChildrenList.setLayoutManager(layoutManager);
            rvWantChildrenList.setAdapter(adapterWantChildren);

    }

    /*  method for add information
     *  Created Date: 24-08-2020
     *  Updated Date:
     */

    private void add_info2() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            //  jsonBody.put("id", "18");
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("marital_id", maritalStatusId);
            jsonBody.put("religion_id", religionId);
            jsonBody.put("has_children_id", childrenId);
            jsonBody.put("wants_children", wantChildrenId);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_INFO2, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                                Intent i = new Intent(context, InfoThird.class);
                                startActivity(i);


                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void getMaritalStatusId(String n) {
        maritalStatusId=n;

    }

    @Override
    public void getWantChildrenId(String n) {
        wantChildrenId=n;
    }

    @Override
    public void getReligionId(String n) {
        religionId=n;
    }

    @Override
    public void getChildrenId(String n) {
        childrenId=n;
    }
}
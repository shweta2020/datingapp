package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.INDIVIDUAL_PROFILE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:31-08-2020
 * Updated Date:03-09-2020(apply api)
 **/
public class IndividualProfile extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;

    //hooks
    LinearLayout llBackActivity;
    TextView tvGender, tvMaritalStatus, tvEmail, tvHeight, tvName, tvLocation;
    TextView tvBodyType, tvReligion, tvSmoking, tvDrinking, tvProfession, tvAboutMe;
    TextView tvInterestedIn, tvEthnicity, tvChildren, tvWantChildren, tvInterests;
    ImageView imgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_profile);
        initialization();
        action();
    }

    public void initialization() {
        context = IndividualProfile.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvGender = findViewById(R.id.tv_gender);
        tvMaritalStatus = findViewById(R.id.tv_marital_status);
        tvEmail = findViewById(R.id.tv_email);
        tvHeight = findViewById(R.id.tv_height);
        tvBodyType = findViewById(R.id.tv_bodyType);
        tvReligion = findViewById(R.id.tv_religion);
        tvSmoking = findViewById(R.id.tv_smoking);
        tvDrinking = findViewById(R.id.tv_drinking);
        tvProfession = findViewById(R.id.tv_profession);
        tvAboutMe = findViewById(R.id.tv_about_me);
        imgProfile = findViewById(R.id.img_Profile);
        tvName = findViewById(R.id.tv_name);
        tvLocation = findViewById(R.id.tv_location);
        tvInterestedIn = findViewById(R.id.tv_interested_in);
        tvEthnicity = findViewById(R.id.tv_ethnicity);
        tvChildren = findViewById(R.id.tv_children);
        tvWantChildren = findViewById(R.id.tv_want_children);
        tvInterests = findViewById(R.id.tv_interests);
    }

    public void action() {
        /* finish activity when click on bake arrow
         * Created Date: 03-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /**
         * call viewProfile method
         * Created Date: 03-09-2020
         **/
        viewProfile();

    }
    /*  volley method for fetching profile data
     *  Created Date: 03-09-2020
     */

    private void viewProfile() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("fnd_id", getIntent().getStringExtra("user_id"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + INDIVIDUAL_PROFILE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");

                            if (status) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                             /*   Glide.with(getApplicationContext())
                                        .load(jsonObject.getString("image"))
                                        .placeholder(R.drawable.user_icon)
                                        .into(imgProfile);*/
                                    tvName.setText(jsonObject.getString("name") + "," + jsonObject.getString("age"));
                                    tvLocation.setText(jsonObject.getString("city_name") + "," + jsonObject.getString("country_name"));
                                    tvGender.setText(jsonObject.getString("gender"));

                                    tvMaritalStatus.setText(jsonObject.getString("marital_status"));
                                    tvEmail.setText(jsonObject.getString("email"));
                                    tvHeight.setText(jsonObject.getString("height"));
                                    tvBodyType.setText(jsonObject.getString("body_type"));
                                    tvReligion.setText(jsonObject.getString("religion"));
                                    tvSmoking.setText(jsonObject.getString("smokeing"));
                                    tvDrinking.setText(jsonObject.getString("drinking"));
                                    tvProfession.setText(jsonObject.getString("profession"));
                                    tvAboutMe.setText(jsonObject.getString("about"));
                                    tvInterestedIn.setText(jsonObject.getString("interested_in"));
                                    tvEthnicity.setText(jsonObject.getString("ethnicity"));
                                    tvChildren.setText(jsonObject.getString("has_children_name"));
                                    tvWantChildren.setText(jsonObject.getString("want_child"));
                                    tvInterests.setText(jsonObject.getString("hoby_name"));
                                }

                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

}
package com.nusys.datingapp.activity.FragmentHome;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.FragmentLike.PageAdapter;

/**
 * Created By: Shweta Agarwal
 * Created DAte:13-08-2020
 * Updated Date: 25-08-2020(add pagination and api)
 **/
public class HomeHome extends Fragment {
    Context context;
    ViewPager viewPager;
    TabLayout tabLayout;
    PagerAdapter pagerAdapter;

    FrameLayout mainFrame, frame_container;
    Fragment fragment;
    LinearLayout llSetting, llLikes, llHome, llMsg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home_home, container, false);

        context = getContext();
        tabLayout = (TabLayout) view.findViewById(R.id.simpleTabLayout);
        mainFrame =view.findViewById(R.id.mainFrameHome);
        frame_container = view.findViewById(R.id.frame_container_home);
        llSetting = view.findViewById(R.id.ll_setting);
        llLikes = view.findViewById(R.id.ll_likes);
        llHome = view.findViewById(R.id.ll_home);
        llMsg = view.findViewById(R.id.ll_msg);
        loadFragment(new HomeHome());
        /* intent to cardStackMatch when click on tvMatchHeader
         * Created Date: 26-08-2020
         */

       /* llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Home.class));
            }
        });
        llLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new LikeHome();
                loadFragment(fragment);
                //  startActivity(new Intent(context, LikesMe.class));
            }
        });

        llMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MessageHome();
                loadFragment(fragment);
                //startActivity(new Intent(context, Message.class));
            }
        });
        llSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MoreFragment();
                loadFragment(fragment);
                // startActivity(new Intent(context, MoreFragment.class));
            }
        });
*/

        tabLayout.addTab(tabLayout.newTab().setText("Meet"));
        tabLayout.addTab(tabLayout.newTab().setText("Match"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.simpleViewPager);
        pagerAdapter = new PageAdapter(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });



        return view;
    }

    /**
     * loadFragment method for loading fragments
     * Created DAte:04-09-2020
     **/
    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            mainFrame.setVisibility(View.GONE);
            frame_container.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container_home, fragment);
            transaction.addToBackStack("CalenderWebview");
            getActivity().getSupportFragmentManager().popBackStackImmediate();
            transaction.commit();

        }
    }
}
package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.LoginModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created By: Shweta Agarwal
 * Created DAte:10-08-2020
 * Updated Date:
 **/
public class Login extends AppCompatActivity {

    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvLogin, tvForgotPwd;
    EditText etEmail, etPassword;
    TextView tvSignup;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialization();
        action();
    }

    public void initialization() {
        context = Login.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvLogin = findViewById(R.id.tv_login);
        tvForgotPwd = findViewById(R.id.tv_forgot_pwd);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        tvSignup = findViewById(R.id.tv_signup);

    }

    public void action() {

        /*redirect to home page when click on btLogin with validation and calling dologin method
         * Created Date: 14-08-2020
         * Updated Date:14-08-2020(apply validation)
         */
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("field can't be empty");

                } else if (!etEmail.getText().toString().matches(emailPattern)) {
                    Toast.makeText(context, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty((etPassword.getText().toString()))) {

                    etPassword.setError("field can't be empty");
                } else {
                    doLogin();

                }


            }
        });
        /*redirect to registration screen when click on tvSignup
         * Created Date: 26-08-2020
         */
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SignupWithPhone.class);
                startActivity(i);
            }
        });

        /*intent to forgot password screen when click on tvForgotPwd
         * Created Date: 28-08-2020
         */
        tvForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ForgotPassword.class);
                startActivity(i);
            }
        });
        ActivityCompat.requestPermissions( Login.this,
                new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }

    }

    /*method for doing login
     * Created Date: 13-08-2020
     * Updated Date: 14-08-2020(change parameter from phone to email)
     */
    private void doLogin() {

        HashMap<String, String> map = new HashMap<>();
        map.put("email", etEmail.getText().toString());
        map.put("password", etPassword.getText().toString());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<LoginModel> call = serviceInterface.doLogin(map);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    LoginModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        sharedPreference_main.setIs_LoggedIn(true);
                        sharedPreference_main.setUserId(bean.getData().getId());
                        sharedPreference_main.setUsername(bean.getData().getName());
                        sharedPreference_main.setgender(bean.getData().getGender());
                        // sharedPreference_main.setUserEmail(bean.getData().get(0).getEmail());
                        sharedPreference_main.setToken(bean.getToken());
                        //  sharedPreference_main.setUserImage(bean.getData().get(0).getProfilePic());

                        Intent i = new Intent(context, Home.class);
                        startActivity(i);
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

                Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

            }
        });


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, Login.class));
        finish();
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                Login.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Login.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                Toast.makeText(this, "Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude, Toast.LENGTH_SHORT).show();
               // showLocation.setText("Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
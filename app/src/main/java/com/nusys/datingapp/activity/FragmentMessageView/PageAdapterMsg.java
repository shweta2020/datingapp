package com.nusys.datingapp.activity.FragmentMessageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PageAdapterMsg extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PageAdapterMsg(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Message tab1 = new Message();
                return tab1;
            case 1:
                ViewedMe tab2 = new ViewedMe();
                return tab2;
                case 2:
                IViewed tab3 = new IViewed();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

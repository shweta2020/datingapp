package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nusys.datingapp.R;
import com.nusys.datingapp.models.ForgotPwdModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:03-09-2020 (apply api)
 **/
public class ForgotPassword extends AppCompatActivity {
    Context context;
    //hooks
    EditText etEmail;
    Button btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initialization();
        action();
    }

    public void initialization() {
        context =ForgotPassword.this;

        etEmail = findViewById(R.id.et_email);
        btSubmit = findViewById(R.id.bt_submit);
    }

    public void action() {

        /**
         * calling method of forgot_password() when do validation
         * Created DAte: 03-09-2020
         * Updated Date:
         **/
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    etEmail.setError("Please enter email");
                } else {
                    forgot_password();
                }
            }
        });
    }
    /* method for forgot password
     *created date: 03-09-2020
     */
    private void forgot_password() {

        HashMap<String, String> map = new HashMap<>();

        map.put("email", etEmail.getText().toString());


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ForgotPwdModel> call = serviceInterface.forgot_pwd(map);
        call.enqueue(new Callback<ForgotPwdModel>() {
            @Override
            public void onResponse(Call<ForgotPwdModel> call, Response<ForgotPwdModel> response) {
                if (response.isSuccessful()) {
                    ForgotPwdModel bean = response.body();
                    Boolean status= bean.getStatus();
                    if (status) {


                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, Login.class));
                        finish();

                    } else {
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotPwdModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrongs", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
package com.nusys.datingapp.activity.FragmentMessageView;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.nusys.datingapp.R;


public class MessageHome extends Fragment {
    Context context;
    ViewPager viewPager;
    TabLayout tabLayout;
    PageAdapterMsg pageAdapterMsg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_message_home, container, false);
        context = getContext();
        tabLayout = (TabLayout) view.findViewById(R.id.simpleTabLayout);

        tabLayout.addTab(tabLayout.newTab().setText("Message"));
        tabLayout.addTab(tabLayout.newTab().setText("Viewed Me"));
        tabLayout.addTab(tabLayout.newTab().setText("I Viewed"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.simpleViewPager);
        pageAdapterMsg = new PageAdapterMsg(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapterMsg);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return view;
    }
}
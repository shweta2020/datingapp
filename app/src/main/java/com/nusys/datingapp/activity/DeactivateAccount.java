package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CHANGE_PWD;
import static com.nusys.datingapp.commonModules.Constants.DEACTIVATE_ACC;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:03-09-2020 (apply api)
 **/
public class DeactivateAccount extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvToolbarHead, tvChangePwd;
    LinearLayout llBackActivity;
    Button btDeactivate;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivate_account);
        initialization();
        action();
    }

    public void initialization() {
        context = DeactivateAccount.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks

        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tvChangePwd = findViewById(R.id.tv_change_pwd);
        llBackActivity = findViewById(R.id.ll_back_activity);
        btDeactivate = findViewById(R.id.bt_deactivate);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Deactivate Account");

        /* finish activity when click on bake arrow
         * Created Date: 28-08-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /**
         * calling method deactivate_acc() when click on btDeactivate
         * Created DAte:03-09-2020
         * Updated Date:
         **/
        btDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deactivate_acc();
            }
        });
    }
    /*  volley method for deactivate acc
     *  Created Date: 03-09-2020
     */
    private void deactivate_acc() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters


            jsonBody.put("id", sharedPreference_main.getUserId());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+DEACTIVATE_ACC, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {
                                sharedPreference_main.removePreference();
                                sharedPreference_main.setIs_LoggedIn(false);
                                startActivity(new Intent(context, SelectionForSignupLogin.class));
                                finish();
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.commonModules.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CHANGE_PWD;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:03-09-2020 (apply api)
 **/
public class ChangePassword extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvToolbarHead;
    LinearLayout llBackActivity;
    EditText etOldPassword,etNewPassword, etConfirmPassword;
    Button btSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initialization();
        action();
    }

    public void initialization() {
        context = ChangePassword.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);
        etOldPassword = findViewById(R.id.et_oldPassword);
        etNewPassword = findViewById(R.id.et_NewPassword);
        etConfirmPassword = findViewById(R.id.et_ConfirmPassword);
        btSubmit = findViewById(R.id.bt_Submit);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Change Password");

        /* finish activity when click on bake arrow
         * Created Date: 28-08-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**
         * calling method of change_pwd() when do validation
         * Created DAte: 03-09-2020
         * Updated Date:
         **/
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etOldPassword.getText().toString())) {
                    etOldPassword.setError("field can't be empty");
                } else if (TextUtils.isEmpty(etNewPassword.getText().toString())) {
                    etNewPassword.setError("field can't be empty");
                } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString())) {
                    etConfirmPassword.setError("field can't be empty");
                } else if (!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                    etConfirmPassword.setError("confirm password not matched");

                } else {
                    change_pwd();
                }
            }
        });
    }
    /*  volley method for change password
     *  Created Date: 03-09-2020
     */
    private void change_pwd() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("old_password", etOldPassword.getText().toString());
            jsonBody.put("new_password", etNewPassword.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+CHANGE_PWD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {
                                startActivity(new Intent(context, Login.class));
                                finish();
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
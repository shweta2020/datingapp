package com.nusys.datingapp.activity.queGetStarted;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.Home;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.ProfessionModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.ADD_INFO3;
import static com.nusys.datingapp.commonModules.Constants.ADD_INFO4;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:19-08-2020
 * Updated Date:09-09-2020(add profession spinner and api)
 **/
public class InfoForth extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvSkip, tvSave, tvAbout;
    LinearLayout llAbout;
    EditText etAbout;
    boolean visibility_maintain = false;

    Spinner spProfession;
    private ArrayList<String> spinProfession = new ArrayList<String>();
    private ArrayList<String> spinProfession_id = new ArrayList<String>();
    String spProfessionID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_forth);
        initialization();
        action();

    }

    public void initialization() {
        context = InfoForth.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvSkip = findViewById(R.id.tv_skip);
        tvSave = findViewById(R.id.tv_save);

        tvAbout = findViewById(R.id.tv_about);
        llAbout = findViewById(R.id.ll_about);

        etAbout = findViewById(R.id.et_about);

        spProfession = findViewById(R.id.sp_profession);
        spinProfession.add("Select Profession");
        spinProfession_id.add("-1");


    }

    public void action() {

        /*skip showing marital status and goes to next page
         * Created Date: 19-08-2020
         */
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, Home.class);
                startActivity(i);
            }
        });
        /*redirect to another page
         * Created Date: 19-08-2020
         * Updated Date: 25-08-2020(calling method add_info4)
         */
        tvSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                add_info4();
             /*   Intent i = new Intent(context, Home.class);
                startActivity(i);*/
            }
        });

        /**
         * maintain visibility for about me
         * Created Date: 25-08-2020
         **/
        tvAbout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {

                    llAbout.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llAbout.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });

        /*  calling view_profession_list method
         *  Created Date: 09-09-2020
         */
        view_profession_list();
        /*  action for taking spinner profession value
         *  Created Date: 09-09-2020
         *  Updated Date:
         */

        spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spProfessionID = spinProfession_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*  method for fetching profession list
     *  Created Date: 09-09-2020
     */

    private void view_profession_list() {


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ProfessionModel> call = serviceInterface.profession_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<ProfessionModel>() {
            @Override
            public void onResponse(Call<ProfessionModel> call, Response<ProfessionModel> response) {
                if (response.isSuccessful()) {
                    ProfessionModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String professionName = bean.getData().get(i).getName();
                            String professionID = bean.getData().get(i).getId();

                            spinProfession.add(professionName);
                            spinProfession_id.add(professionID);

                        }
                    } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spProfession.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinProfession));
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfessionModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


    }
    /*  method for add information
     *  Created Date: 25-08-2020
     *  Updated Date:09-09-2020(change profession from editbox to spinner and add profession id)
     */

    private void add_info4() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {


            //  jsonBody.put("id", "18");
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("profession", spProfessionID);
            jsonBody.put("about", etAbout.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_INFO4, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                                Intent i = new Intent(context, Home.class);
                                startActivity(i);

                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }
}
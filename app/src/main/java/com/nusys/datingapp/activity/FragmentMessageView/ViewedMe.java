package com.nusys.datingapp.activity.FragmentMessageView;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterWhoViewed;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.WhoViewedModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:04-09-2020
 * Updated Date:
 **/
public class ViewedMe extends Fragment {

    SharedPreference_main sharedPreference_main;
    RecyclerView rvWhoViewed;
    AdapterWhoViewed adapterWhoViewed;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_viewed_me, container, false);
        initialization(view);
        action();
        return view;
    }

    public void initialization(View view) {

        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        rvWhoViewed = view.findViewById(R.id.rv_who_viewed);

    }

    public void action() {
        /**
         * calling retrofit method mymatch()
         * Created DAte:04-09-2020
         * Updated Date:
         **/
        whoViewedList();
    }

    /**
     * method for fetching who viewed list
     * Created Date: 04-09-2020
     **/

    private void whoViewedList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", sharedPreference_main.getUserId());
        map.put("age_from", "18");
        map.put("age_to", "30");
        map.put("city_name","delhi");
        map.put("page_no", "1");

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<WhoViewedModel> call = serviceInterface.who_viewed_me_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<WhoViewedModel>() {
            @Override
            public void onResponse(Call<WhoViewedModel> call, Response<WhoViewedModel> response) {
                if (response.isSuccessful()) {
                    WhoViewedModel bean = response.body();
                    if (bean.getStatus().equals(true)) {
                        adapterWhoViewed = new AdapterWhoViewed(getContext(), bean.getData());
                        rvWhoViewed.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                        rvWhoViewed.setItemAnimator(new DefaultItemAnimator());
                        rvWhoViewed.setAdapter(adapterWhoViewed);
                        Log.e("Group_response", bean.toString());

                    } else {
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<WhoViewedModel> call, Throwable t) {

            }
        });


    }
}
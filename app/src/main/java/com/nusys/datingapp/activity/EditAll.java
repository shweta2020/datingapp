package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterBodyType;
import com.nusys.datingapp.adapter.AdapterChildren;
import com.nusys.datingapp.adapter.AdapterDrinks;
import com.nusys.datingapp.adapter.AdapterEthnicity;
import com.nusys.datingapp.adapter.AdapterInterestedIn;
import com.nusys.datingapp.adapter.AdapterInterests;
import com.nusys.datingapp.adapter.AdapterMaritalStatus;
import com.nusys.datingapp.adapter.AdapterReligion;
import com.nusys.datingapp.adapter.AdapterSmokes;
import com.nusys.datingapp.adapter.AdapterWantChildren;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetBodyTypeIds;
import com.nusys.datingapp.interfaces.GetChildrenIds;
import com.nusys.datingapp.interfaces.GetDrinksIds;
import com.nusys.datingapp.interfaces.GetEthnicityIds;
import com.nusys.datingapp.interfaces.GetInterestedInIds;
import com.nusys.datingapp.interfaces.GetInterestsIds;
import com.nusys.datingapp.interfaces.GetMaritalStatusIds;
import com.nusys.datingapp.interfaces.GetReligionIds;
import com.nusys.datingapp.interfaces.GetSmokesIds;
import com.nusys.datingapp.interfaces.GetWantChildrenIds;
import com.nusys.datingapp.models.AllProfileFetchModel;
import com.nusys.datingapp.models.HeightModel;
import com.nusys.datingapp.models.InterestedInModel;
import com.nusys.datingapp.models.ProfessionModel;
import com.nusys.datingapp.models.WantChildrenModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.UPDATE_PROFILE;
import static com.nusys.datingapp.commonModules.Extension.openCalender;

/**
 * Created By: Shweta Agarwal
 * Created DAte:02-09-2020
 * Updated Date:08-09-2020(add edit_profile api)
 * Updated Date:08-09-2020(make all edit pages on one screen)
 **/
public class EditAll extends AppCompatActivity implements GetBodyTypeIds, GetChildrenIds,
        GetDrinksIds, GetEthnicityIds, GetInterestedInIds, GetInterestsIds,
        GetMaritalStatusIds, GetReligionIds, GetSmokesIds, GetWantChildrenIds {

    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    RelativeLayout rlBodyType, rlChildren, rlDrinks, rlEthnicity, rlInterestedIn, rlInterests;
    RelativeLayout rlMaritalStatus, rlReligion, rlSmokes, rlWantChild, rlHeight;
    RelativeLayout rlProfession, rlAbout, rlDob;
    TextView tvSave;

    RecyclerView rvBodyTypeList;
    AdapterBodyType adapterBodyType;
    String bodyTypeId;

    RecyclerView rvChildrenList;
    AdapterChildren adapterChildren;
    String childrenId;

    RecyclerView rvDrinksList;
    AdapterDrinks adapterDrinks;
    String drinksId;

    RecyclerView rvEthnicityList;
    AdapterEthnicity adapterEthnicity;
    String ethnicityId;

    RecyclerView rvInterestedList;
    AdapterInterestedIn adapterInterestedIn;
    String interestedInId;

    RecyclerView rvInterestsList;
    AdapterInterests adapterInterests;
    String hobbyId;
    ArrayList<String> ids;

    RecyclerView rvMaritalStatusList;
    AdapterMaritalStatus adapterMaritalStatus;
    String maritalStatusId;

    RecyclerView rvReligionList;
    AdapterReligion adapterReligion;
    String religionId;

    RecyclerView rvSmokesList;
    AdapterSmokes adapterSmokes;
    String smokesId;

    RecyclerView rvWantChildrenList;
    AdapterWantChildren adapterWantChildren;
    String wantChildrenId;

    EditText etAbout;

    Spinner spHeight;
    private ArrayList<String> spinHeight = new ArrayList<String>();
    private ArrayList<String> spinHeight_id = new ArrayList<String>();
    String spHeightID;

    Spinner spProfession;
    private ArrayList<String> spinProfession = new ArrayList<String>();
    private ArrayList<String> spinProfession_id = new ArrayList<String>();
    String spProfessionID;

    private int mYear, mMonth, mDay;
    TextView tvDob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_all);
        initialization();
        action();
    }

    public void initialization() {
        context = EditAll.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        //Hooks
        tvSave = findViewById(R.id.tv_save);

        rvBodyTypeList = findViewById(R.id.rv_bodytype_list);
        rlBodyType = findViewById(R.id.rl_body_Type);

        rvChildrenList = findViewById(R.id.rv_children_list);
        rlChildren = findViewById(R.id.rl_children);

        rvDrinksList = findViewById(R.id.rv_drinks_List);
        rlDrinks = findViewById(R.id.rl_drinks);

        rvEthnicityList = findViewById(R.id.rv_ethnicity_list);
        rlEthnicity = findViewById(R.id.rl_ethnicity);

        rvInterestedList = findViewById(R.id.rv_interestedList);
        rlInterestedIn = findViewById(R.id.rl_interested_In);

        rvInterestsList = findViewById(R.id.rv_interests_list);
        rlInterests = findViewById(R.id.rl_interests);

        rvMaritalStatusList = findViewById(R.id.rv_marital_status_List);
        rlMaritalStatus = findViewById(R.id.rl_maritalStatus);

        rvReligionList = findViewById(R.id.rv_religion_List);
        rlReligion = findViewById(R.id.rl_religion);

        rvSmokesList = findViewById(R.id.rv_smokes_List);
        rlSmokes = findViewById(R.id.rl_smokes);

        rvWantChildrenList = findViewById(R.id.rv_want_children_list);
        rlWantChild = findViewById(R.id.rl_wantChild);

        rlAbout = findViewById(R.id.rl_about);
        etAbout = findViewById(R.id.et_about);

        rlHeight = findViewById(R.id.rl_height);
        spHeight = findViewById(R.id.sp_height);
        spinHeight.add("Select Height");
        spinHeight_id.add("-1");

        rlProfession = findViewById(R.id.rl_profession);
        spProfession = findViewById(R.id.sp_profession);
        spinProfession.add("Select Profession");
        spinProfession_id.add("-1");

        rlDob = findViewById(R.id.rl_dob);
        tvDob = findViewById(R.id.tv_dob);

    }

    public void action() {


        /*redirect to another page
         * Created Date: 02-09-2020
         */
        tvSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                update_profile();

            }
        });
        /**
         * call fetch_body_type method
         * Created Date: 02-09-2020
         * Updated Date: 08-9-2020(apply conditions)
         **/
        if (getIntent().getStringExtra("updateType").equals("body_type")) {
            rlBodyType.setVisibility(View.VISIBLE);
            fetch_body_type();
        } else if (getIntent().getStringExtra("updateType").equals("children")) {
            rlChildren.setVisibility(View.VISIBLE);
            fetch_children();
        } else if (getIntent().getStringExtra("updateType").equals("dob")) {
            rlDob.setVisibility(View.VISIBLE);
            /* calling date picker when click on dob
             *  Created Date: 09-09-2020
             */

            tvDob.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View v) {
                    openCalender(context, mYear, mMonth, mDay, tvDob);

                }

            });

        } else if (getIntent().getStringExtra("updateType").equals("drinks")) {
            rlDrinks.setVisibility(View.VISIBLE);
            fetch_drinks();
        } else if (getIntent().getStringExtra("updateType").equals("ethnicity")) {
            rlEthnicity.setVisibility(View.VISIBLE);
            fetch_ethnicity();
        } else if (getIntent().getStringExtra("updateType").equals("interested")) {
            rlInterestedIn.setVisibility(View.VISIBLE);
            fetch_interested_in();
        } else if (getIntent().getStringExtra("updateType").equals("hobby")) {
            rlInterests.setVisibility(View.VISIBLE);
            fetch_interests();
        } else if (getIntent().getStringExtra("updateType").equals("marital_status")) {
            rlMaritalStatus.setVisibility(View.VISIBLE);
            fetch_marital_status();
        } else if (getIntent().getStringExtra("updateType").equals("religion")) {
            rlReligion.setVisibility(View.VISIBLE);
            fetch_religion();
        } else if (getIntent().getStringExtra("updateType").equals("smokes")) {
            rlSmokes.setVisibility(View.VISIBLE);
            fetch_smokes();
        } else if (getIntent().getStringExtra("updateType").equals("want_children")) {
            rlWantChild.setVisibility(View.VISIBLE);
            fetch_want_children();
        } else if (getIntent().getStringExtra("updateType").equals("about_me")) {
            rlAbout.setVisibility(View.VISIBLE);
        } else if (getIntent().getStringExtra("updateType").equals("height")) {
            rlHeight.setVisibility(View.VISIBLE);
            /*take value of slider
             * Created Date: 01-09-2020
             * Updated Date:09-09-2020(change slider to spinner)
             * Updated Date:09-09-2020(calling view_profession_list method)
             */
            view_height_list();
            /*  action for taking spinner profession value
             *  Created Date: 08-09-2020
             *  Updated Date:
             */

            spHeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    spHeightID = spinHeight_id.get(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }else if(getIntent().getStringExtra("updateType").equals("profession")){
            rlProfession.setVisibility(View.VISIBLE);
            /*  calling view_profession_list method
             *  Created Date: 08-09-2020
             */
            view_profession_list();
            /*  action for taking spinner profession value
             *  Created Date: 08-09-2020
             *  Updated Date:
             */

            spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    spProfessionID = spinProfession_id.get(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    }

    /*  retrofit method for fetching body type list
     *  Created Date: 02-09-2020
     */
    private void fetch_body_type() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_body_type(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterBodyType = new AdapterBodyType(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvBodyTypeList.setLayoutManager(layoutManager);
                        rvBodyTypeList.setAdapter(adapterBodyType);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching children list
     *  Created Date: 21-08-2020
     */

    private void fetch_children() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_children(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterChildren = new AdapterChildren(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvChildrenList.setLayoutManager(layoutManager);
                        rvChildrenList.setAdapter(adapterChildren);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching religion list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */
    private void fetch_drinks() {
        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0", "Any"));
        list.add(new WantChildrenModel("1", "No"));
        list.add(new WantChildrenModel("2", "Occasionally"));
        list.add(new WantChildrenModel("3", "Often"));


        adapterDrinks = new AdapterDrinks(context, (list), EditAll.this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvDrinksList.setLayoutManager(layoutManager);
        rvDrinksList.setAdapter(adapterDrinks);


    }

    /*  retrofit method for fetching ethnicity list
     *  Created Date: 20-08-2020
     *  Updated Date:
     */
    private void fetch_ethnicity() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_ethnicity(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterEthnicity = new AdapterEthnicity(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvEthnicityList.setLayoutManager(layoutManager);
                        rvEthnicityList.setAdapter(adapterEthnicity);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching interested in list
     *  Created Date: 02-09-2020
     */
    private void fetch_interested_in() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_interested_in(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterInterestedIn = new AdapterInterestedIn(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvInterestedList.setLayoutManager(layoutManager);
                        rvInterestedList.setAdapter(adapterInterestedIn);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    /*  retrofit method for fetching children list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(add valid api name)
     */

    private void fetch_interests() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_interests(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterInterests = new AdapterInterests(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvInterestsList.setLayoutManager(layoutManager);
                        rvInterestsList.setAdapter(adapterInterests);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching marital status in list
     *  Created Date: 02-09-2020
     */
    private void fetch_marital_status() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_marital_status(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterMaritalStatus = new AdapterMaritalStatus(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvMaritalStatusList.setLayoutManager(layoutManager);
                        rvMaritalStatusList.setAdapter(adapterMaritalStatus);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching religion list
     *  Created Date: 21-08-2020
     */
    private void fetch_religion() {
        HashMap<String, String> map = new HashMap<>();

        map.put("id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<AllProfileFetchModel> call = serviceInterface.fetch_religion(sharedPreference_main.getToken(), CONTENT_TYPE,map);
        call.enqueue(new Callback<AllProfileFetchModel>() {


            @Override
            public void onResponse(Call<AllProfileFetchModel> call, retrofit2.Response<AllProfileFetchModel> response) {

                if (response.isSuccessful()) {

                    AllProfileFetchModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterReligion = new AdapterReligion(context, (bean.getData()), EditAll.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvReligionList.setLayoutManager(layoutManager);
                        rvReligionList.setAdapter(adapterReligion);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllProfileFetchModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching smokes option in list
     *  Created Date: 21-08-2020
     * Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */
    private void fetch_smokes() {

        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0", "Any"));
        list.add(new WantChildrenModel("1", "No"));
        list.add(new WantChildrenModel("2", "Occasionally"));
        list.add(new WantChildrenModel("3", "Often"));


        adapterSmokes = new AdapterSmokes(context, (list), EditAll.this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvSmokesList.setLayoutManager(layoutManager);
        rvSmokesList.setAdapter(adapterSmokes);

    }
    /*  retrofit method for fetching want children list
     *  Created Date: 21-08-2020
     *  Updated Date: 24-08-2020(set adapter from dynamic to static data)
     */

    private void fetch_want_children() {

        final List<WantChildrenModel> list = new ArrayList<>();

        list.add(new WantChildrenModel("0", "Any"));
        list.add(new WantChildrenModel("1", "No"));
        list.add(new WantChildrenModel("2", "Yes"));
        list.add(new WantChildrenModel("3", "Undecided/Open"));

        adapterWantChildren = new AdapterWantChildren(context, (list), EditAll.this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvWantChildrenList.setLayoutManager(layoutManager);
        rvWantChildrenList.setAdapter(adapterWantChildren);

    }

    /*  method for fetching profession list
     *  Created Date: 08-09-2020
     */

    private void view_profession_list() {


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ProfessionModel> call = serviceInterface.profession_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<ProfessionModel>() {
            @Override
            public void onResponse(Call<ProfessionModel> call, Response<ProfessionModel> response) {
                if (response.isSuccessful()) {
                    ProfessionModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String professionName = bean.getData().get(i).getName();
                            String professionID = bean.getData().get(i).getId();

                            spinProfession.add(professionName);
                            spinProfession_id.add(professionID);

                        }
                        } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spProfession.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinProfession));
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfessionModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


    }
    /*  method for fetching height list
     *  Created Date: 09-09-2020
     */

    private void view_height_list() {


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<HeightModel> call = serviceInterface.height_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<HeightModel>() {
            @Override
            public void onResponse(Call<HeightModel> call, Response<HeightModel> response) {
                if (response.isSuccessful()) {
                    HeightModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String heightName = bean.getData().get(i).getName();
                            String heightID = bean.getData().get(i).getId();

                            spinHeight.add(heightName);
                            spinHeight_id.add(heightID);

                        }
                    } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spHeight.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinHeight));
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HeightModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


    }
    /*  volley method for update_profile
     *  Created Date: 08-09-2020
     *  Updated Date:
     */
    private void update_profile() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("update_type", getIntent().getStringExtra("updateType"));
            if (getIntent().getStringExtra("updateType").equals("body_type")) {
                jsonBody.put("body_type", bodyTypeId);
            } else if (getIntent().getStringExtra("updateType").equals("children")) {
                jsonBody.put("has_children_id", childrenId);
            } else if (getIntent().getStringExtra("updateType").equals("dob")) {
                jsonBody.put("dob", tvDob.getText().toString());
            }else if (getIntent().getStringExtra("updateType").equals("drinks")) {
                jsonBody.put("drinks", drinksId);
            } else if (getIntent().getStringExtra("updateType").equals("ethnicity")) {
                jsonBody.put("ethnicity_id", ethnicityId);
            } else if (getIntent().getStringExtra("updateType").equals("interested")) {
                jsonBody.put("interested_id", interestedInId);
            } else if (getIntent().getStringExtra("updateType").equals("hobby")) {
                jsonBody.put("hobby_id", hobbyId);
            } else if (getIntent().getStringExtra("updateType").equals("marital_status")) {
                jsonBody.put("marital_id", maritalStatusId);
            } else if (getIntent().getStringExtra("updateType").equals("religion")) {
                jsonBody.put("religion_id", religionId);
            } else if (getIntent().getStringExtra("updateType").equals("smokes")) {
                jsonBody.put("smokes", smokesId);
            } else if (getIntent().getStringExtra("updateType").equals("want_children")) {
                jsonBody.put("wants_children", wantChildrenId);
            } else if (getIntent().getStringExtra("updateType").equals("height")) {
                jsonBody.put("height_id", spHeightID);
            }else if (getIntent().getStringExtra("updateType").equals("profession")) {
                jsonBody.put("profession", spProfessionID);
            }else if (getIntent().getStringExtra("updateType").equals("about_me")) {
                jsonBody.put("about", etAbout.getText().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + UPDATE_PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                Intent i = new Intent(context, EditProfile.class);
                                startActivity(i);
                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    @Override
    public void getBodyTypeId(String n) {
        bodyTypeId = n;
    }

    @Override
    public void getChildrenId(String n) {
        childrenId = n;
    }

    @Override
    public void getDrinksId(String n) {
        drinksId = n;
    }

    @Override
    public void getEthnicityId(String n) {
        ethnicityId = n;
    }

    @Override
    public void getInterestedId(String n) {
        interestedInId = n;
    }

    @Override
    public void getInterestsId(ArrayList<String> ids) {
        this.ids = ids;
        Log.e("ids", ids.toString());
        String P_id = "";
        StringBuilder pd = new StringBuilder();
        for (Object mPI : ids) {
            pd.append(P_id).append(mPI);
            P_id = ",";
        }
        hobbyId = pd.toString();
        // Toast.makeText(context, ""+hobbyId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getMaritalStatusId(String n) {
        maritalStatusId = n;
    }

    @Override
    public void getReligionId(String n) {
        religionId = n;
    }

    @Override
    public void getSmokesId(String n) {

        smokesId = n;
    }

    @Override
    public void getWantChildrenId(String n) {
        wantChildrenId = n;
    }
}
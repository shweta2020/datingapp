package com.nusys.datingapp.activity.queGetStarted;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.material.slider.Slider;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.ProfilePicUpload;
import com.nusys.datingapp.adapter.AdapterBodyType;
import com.nusys.datingapp.adapter.AdapterEthnicity;
import com.nusys.datingapp.adapter.AdapterInterestedIn;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetBodyTypeIds;
import com.nusys.datingapp.interfaces.GetEthnicityIds;
import com.nusys.datingapp.interfaces.GetInterestedInIds;
import com.nusys.datingapp.models.HeightModel;
import com.nusys.datingapp.models.InterestedInModel;
import com.nusys.datingapp.models.LoginModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.ADD_COUNTRY_CITY;
import static com.nusys.datingapp.commonModules.Constants.ADD_INFO1;
import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:19-08-2020
 * Updated Date:20-08-2020(add api for intrested in)
 * Updated Date:21-08-2020(add api for body type, ethnicity and add visibility)
 **/
public class InfoFirst extends AppCompatActivity implements GetInterestedInIds, GetBodyTypeIds, GetEthnicityIds {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvSkip, tvSave;
    Spinner spHeight;
    private ArrayList<String> spinHeight = new ArrayList<String>();
    private ArrayList<String> spinHeight_id = new ArrayList<String>();
    String spHeightID;


    TextView tvInterestedIn, tvHeightView, tvBodyType, tvEthnicity;
    LinearLayout llHeight, llBodyType, llEthnicity;
    boolean visibility_maintain = false;

    RecyclerView rvInterestedList, rvBodyTypeList, rvEthnicityList;
    AdapterInterestedIn adapterInterestedIn;
    AdapterBodyType adapterBodyType;
    AdapterEthnicity adapterEthnicity;

    String interestedInId,bodyTypeId, ethnicityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_first);
        initialization();
        action();

    }

    public void initialization() {
        context = InfoFirst.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        //Hooks
        tvSkip = findViewById(R.id.tv_skip);
        tvSave = findViewById(R.id.tv_save);

        spHeight = findViewById(R.id.sp_height);
        spinHeight.add("Select Height");
        spinHeight_id.add("-1");

        tvInterestedIn = findViewById(R.id.tv_interested_in);
        tvHeightView = findViewById(R.id.tv_height_view);
        tvBodyType = findViewById(R.id.tv_bodytype);
        tvEthnicity = findViewById(R.id.tv_ethnicity);

        llHeight = findViewById(R.id.ll_height);
        llBodyType = findViewById(R.id.ll_body_type);
        llEthnicity = findViewById(R.id.ll_ethnicity);

        rvInterestedList = findViewById(R.id.rv_interestedList);
        rvBodyTypeList = findViewById(R.id.rv_bodytype_list);
        rvEthnicityList = findViewById(R.id.rv_ethnicity_list);
    }

    public void action() {

        /*take value of slider
         * Created Date: 19-08-2020
         *  Updated dat: 21-08-2020(add cm )
         * Updated Date:09-09-2020(change slider to spinner)
         * Updated Date:09-09-2020(calling view_profession_list method)
         */
        view_height_list();
        /*  action for taking spinner profession value
         *  Created Date: 08-09-2020
         *  Updated Date:
         */

        spHeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spHeightID = spinHeight_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*skip showing marital status and goes to next page
         * Created Date: 19-08-2020
         */
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, InfoSecond.class);
                startActivity(i);
            }
        });
        /*redirect to another page
         * Created Date: 19-08-2020
         * Updated Date: 24-08-2020(call add_info1 method)
         */
        tvSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                add_info1();
               /* Intent i = new Intent(context, InfoSecond.class);
                startActivity(i);*/
            }
        });
        /**
         * call fetch_interested_in method
         * Created Date: 21-08-2020
         **/
        fetch_interested_in();

        /**
         * maintain visibility for height
         * Created Date: 21-08-2020
         **/
        tvHeightView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    llHeight.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llHeight.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });

        /**
         * maintain visibility for body type and call fetch_body_type method
         * Created Date: 21-08-2020
         **/
        tvBodyType.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_body_type();
                    llBodyType.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llBodyType.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });
        /**
         * maintain visibility for ethnicity and call fetch_ethnicity method
         * Created Date: 21-08-2020
         **/
        tvEthnicity.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (visibility_maintain == true) {
                    fetch_ethnicity();
                    llEthnicity.setVisibility(View.VISIBLE);
                    visibility_maintain = false;
                } else {
                    llEthnicity.setVisibility(View.GONE);
                    visibility_maintain = true;

                }

            }
        });
    }

    /*  retrofit method for fetching interested in list
     *  Created Date: 20-08-2020
     *  Updated Date: 20-08-2020(apply flexbox recycler layout)
     */
    private void fetch_interested_in() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_interested_in(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterInterestedIn = new AdapterInterestedIn(context, (bean.getData()),InfoFirst.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvInterestedList.setLayoutManager(layoutManager);
                        rvInterestedList.setAdapter(adapterInterestedIn);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching body type list
     *  Created Date: 21-08-2020
     */
    private void fetch_body_type() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_body_type(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterBodyType = new AdapterBodyType(context, (bean.getData()),InfoFirst.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvBodyTypeList.setLayoutManager(layoutManager);
                        rvBodyTypeList.setAdapter(adapterBodyType);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*  retrofit method for fetching ethnicity list
     *  Created Date: 20-08-2020
     *  Updated Date:
     */
    private void fetch_ethnicity() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<InterestedInModel> call = serviceInterface.fetch_ethnicity(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<InterestedInModel>() {


            @Override
            public void onResponse(Call<InterestedInModel> call, retrofit2.Response<InterestedInModel> response) {

                if (response.isSuccessful()) {

                    InterestedInModel bean = response.body();

                    if (bean.getStatus().equals(true)) {

                        adapterEthnicity = new AdapterEthnicity(context, (bean.getData()),InfoFirst.this);
                        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
                        layoutManager.setFlexDirection(FlexDirection.ROW);
                        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
                        rvEthnicityList.setLayoutManager(layoutManager);
                        rvEthnicityList.setAdapter(adapterEthnicity);
                        Log.e("Group_response", bean.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestedInModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    /*  method for fetching height list
     *  Created Date: 09-09-2020
     */

    private void view_height_list() {


        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<HeightModel> call = serviceInterface.height_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<HeightModel>() {
            @Override
            public void onResponse(Call<HeightModel> call, Response<HeightModel> response) {
                if (response.isSuccessful()) {
                    HeightModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String heightName = bean.getData().get(i).getName();
                            String heightID = bean.getData().get(i).getId();

                            spinHeight.add(heightName);
                            spinHeight_id.add(heightID);

                        }
                    } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spHeight.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinHeight));
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HeightModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


    }
    /*  method for add information
     *  Created Date: 24-08-2020
     *  Updated Date:
     */

    private void add_info1() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            //  jsonBody.put("id", "18");
            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("interested_id", interestedInId);
            jsonBody.put("height_id", spHeightID);
            jsonBody.put("body_id", bodyTypeId);
            jsonBody.put("ethnicity_id", ethnicityId);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_INFO1, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                                Intent i = new Intent(context, InfoSecond.class);
                                startActivity(i);


                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    /*  method for fetching ids via interface
     *  Created Date: 24-08-2020
     *  Updated Date:
     */

    @Override
    public void getInterestedId(String n) {
        interestedInId=n;
    }

    @Override
    public void getBodyTypeId(String n) {
        bodyTypeId=n;
    }

    @Override
    public void getEthnicityId(String n) {
        ethnicityId=n;
    }
}
package com.nusys.datingapp.activity.FragmentLike;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterILiked;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.ILikedModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
/**
 * Created By: Shweta Agarwal
 * Created DAte:27-08-2020
 * Updated Date: 07-08-2020(change activity to fragment)
 **/
public class ILiked extends Fragment {
    SharedPreference_main sharedPreference_main;
    RecyclerView rvILiked;
    AdapterILiked adapterILiked;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_match, container, false);
        initialization(view);
        action();
        return view;
    }

    public void initialization(View view) {
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        //Hooks
        rvILiked = view.findViewById(R.id.rv_iLiked);

    }

    public void action() {

        mymatch();
    }
    /**
     * method for fetching city list
     * Created Date: 20-08-2020
     **/

    private void mymatch() {

        HashMap<String, String> map = new HashMap<>();
        map.put("id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ILikedModel> call = serviceInterface.i_liked_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<ILikedModel>() {
            @Override
            public void onResponse(Call<ILikedModel> call, Response<ILikedModel> response) {
                if (response.isSuccessful()) {
                    ILikedModel bean = response.body();
                    if (bean.getStatus().equals(true)) {
                        adapterILiked = new AdapterILiked(getContext(), bean.getData());
                        rvILiked.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                        rvILiked.setItemAnimator(new DefaultItemAnimator());
                        rvILiked.setAdapter(adapterILiked);
                       /* adapterMyMatch = new AdapterILiked(getContext(), (bean.getData()));
                        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        //layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
                      //  rvMyMatch.setHasFixedSize(true);
                        rvMyMatch.setLayoutManager(layoutManager);
                        rvMyMatch.setAdapter(adapterMyMatch);*/
                        Log.e("Group_response", bean.toString());

                    } else {
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ILikedModel> call, Throwable t) {

            }
        });


    }
}
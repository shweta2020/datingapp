package com.nusys.datingapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nusys.datingapp.R;
/**
 * Created By: Shweta Agarwal
 * Created DAte:10-08-2020
 * Updated Date:
 **/
public class SelectionForSignupLogin extends AppCompatActivity {
    Context context;
    //hooks
    TextView tvSignup, tvLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_for_signup_login);
        initialization();
        action();
    }

    public void initialization() {
        context = SelectionForSignupLogin.this;
        //Hooks
        tvSignup = findViewById(R.id.tv_signup);
        tvLogin = findViewById(R.id.tv_login);
    }

    public void action() {
        /*redirect to selection page where select signup option when click on tvSignup
         * Created Date: 27-07-2020
         */
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SignupWithPhone.class);
                startActivity(i);
            }
        });
        /*redirect to login page when click on tvLogin
         * Created Date: 27-07-2020
         */
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Login.class);
                startActivity(i);
            }
        });
    }

}
package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterLikes;
import com.nusys.datingapp.adapter.AdapterPics;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.interfaces.GetPicDeleteIds;
import com.nusys.datingapp.models.LikesModel;
import com.nusys.datingapp.models.PicsModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.datingapp.commonModules.Constants.CANCEL_PLAN;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.DELETE_PIC;
import static com.nusys.datingapp.commonModules.Constants.GALLERY_REQUEST_CODE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:15-09-2020
 * Updated Date:16-09-2020(apply delete api)
 **/
public class UploadImage extends AppCompatActivity implements GetPicDeleteIds {
    SharedPreference_main sharedPreference_main;
    Context context;
    //hooks
    TextView tvToolbarHead;
    LinearLayout llBackActivity;
    ImageView imgSelector, imgSet;
    //image
    private String userImageBase64;

    RecyclerView rvPicList;
    AdapterPics adapterPics;

    Spinner spImageType;
    int SpinnerId;
    String[] list = {"Public", "Private"};
    String picId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        initialization();
        action();
    }

    public void initialization() {
        sharedPreference_main = SharedPreference_main.getInstance(context);
        context = UploadImage.this;
        //Hooks
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);
        imgSelector = findViewById(R.id.img_selector);
        imgSet = findViewById(R.id.img_set);

        rvPicList = findViewById(R.id.rv_PicList);
        spImageType = findViewById(R.id.sp_imageType);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 16-09-2020
         */
        tvToolbarHead.setText("Images");

        /* finish activity when click on bake arrow
         * Created Date: 16-09-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*calling method open chooser for uploading image
         * Created Date: 11-08-2020
         */
        imgSelector.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);
            }

        });
        /**
         * calling retrofit method myPics()
         * Created DAte:15-09-2020
         * Updated Date:
         **/
        myPics();

        /*  action for taking spinner spGender value
         *  Created Date: 16-09-2020
         *  Updated Date:
         */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, list);
        spImageType.setAdapter(arrayAdapter);
        spImageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // String value = spGender.getItemAtPosition(position).toString();
                SpinnerId = spImageType.getSelectedItemPosition() + 2;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * retrofit method for fetching likes list
     * Created Date: 15-09-2020
     **/

    private void myPics() {

        HashMap<String, String> map = new HashMap<>();
        map.put("id", sharedPreference_main.getUserId());
        map.put("image_type", "");

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<PicsModel> call = serviceInterface.pics_list(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<PicsModel>() {
            @Override
            public void onResponse(Call<PicsModel> call, Response<PicsModel> response) {
                if (response.isSuccessful()) {
                    PicsModel bean = response.body();
                    if (bean.getStatus().equals(true)) {
                        adapterPics = new AdapterPics(context, bean.getData(),UploadImage.this);
                        rvPicList.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                        rvPicList.setItemAnimator(new DefaultItemAnimator());
                        rvPicList.setAdapter(adapterPics);
                       /* adapterMyMatch = new AdapterLikes(context, (bean.getData()));
                        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        //layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
                      //  rvMyLikes.setHasFixedSize(true);
                        rvMyLikes.setLayoutManager(layoutManager);
                        rvMyLikes.setAdapter(adapterMyMatch);*/
                        Log.e("Group_response", bean.toString());

                    } else {
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<PicsModel> call, Throwable t) {

            }
        });


    }
    /*  method for open gallery and camera for uploading image- below all 6 methods
     *  Created Date: 15-09-2020
     */

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            //  fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imgSet.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    imgSet.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();

            }
        });
    }

    @Override
    public void getPicDeleteId(String n) {
        picId=n;
        /*  calling method for delete pic
         *  Created Date: 16-09-2020
         */
        delete_pic();
    }

    /*  volley method for delete pic
     *  Created Date: 16-09-2020
     */
    private void delete_pic() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters

            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("image_id", picId);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL+DELETE_PIC, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                                myPics();
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(ChangePassword.this, ""+response.toString(), Toast.LENGTH_SHORT).show();

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

}
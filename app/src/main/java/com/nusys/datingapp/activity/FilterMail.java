package com.nusys.datingapp.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.nusys.datingapp.R;
import com.nusys.datingapp.adapter.AdapterSmokes;
import com.nusys.datingapp.adapter.AdapterWantChildren;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.HeightModel;
import com.nusys.datingapp.models.WantChildrenModel;
import com.nusys.datingapp.models.WithInModel;
import com.nusys.datingapp.retrofit.ApiClient;
import com.nusys.datingapp.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.datingapp.commonModules.Constants.BASE_URL;
import static com.nusys.datingapp.commonModules.Constants.CONTENT_TYPE;
import static com.nusys.datingapp.commonModules.Constants.MAIL_PRIVACY;
import static com.nusys.datingapp.commonModules.Constants.UPDATE_PROFILE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:28-08-2020
 * Updated Date:14-09-2020(apply apis and functionality)
 **/
public class FilterMail extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    //hooks
    TextView tvToolbarHead;
    LinearLayout llBackActivity;

    Spinner spGender;
    int GenderId;
    String[] list = {"Male", "Female", "Both"};

    CheckBox cbHasChildren, cbMarried, cbSmoke, cbDrink;
    String cbHasChildrenVal, cbMarriedVal, cbSmokeVal, cbDrinkVal;

    Spinner spWithIn;
    private ArrayList<String> spinWithIn = new ArrayList<String>();
    private ArrayList<Integer> spinWithIn_id = new ArrayList<Integer>();
    Integer spWithInID;

    EditText etAgeFrom, etAgeTo;
    Button bt_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_mail);
        initialization();
        action();
    }

    public void initialization() {
        context = FilterMail.this;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        //Hooks
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        llBackActivity = findViewById(R.id.ll_back_activity);

        spGender = findViewById(R.id.sp_gender);

        cbHasChildren = findViewById(R.id.cb_hasChildren);
        cbMarried = findViewById(R.id.cb_married);
        cbSmoke = findViewById(R.id.cb_smoke);
        cbDrink = findViewById(R.id.cb_drink);

        etAgeFrom = findViewById(R.id.et_ageFrom);
        etAgeTo = findViewById(R.id.et_ageTo);

        spWithIn = findViewById(R.id.sp_withIn);
        spinWithIn.add("Select Distance");
        spinWithIn_id.add(-1);

        bt_submit = findViewById(R.id.btSubmit);
    }

    public void action() {
        /* set text heading on tool bar
         * Created Date: 28-08-2020
         */
        tvToolbarHead.setText("Set Filter");

        /* finish activity when click on bake arrow
         * Created Date: 28-08-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*  action for taking spinner spGender value
         *  Created Date: 14-09-2020
         *  Updated Date:
         */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, list);
        spGender.setAdapter(arrayAdapter);
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // String value = spGender.getItemAtPosition(position).toString();
                GenderId = spGender.getSelectedItemPosition() + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  setting checkbox value
         *  Created Date: 14-09-2020
         *  Updated Date:
         */

        if (cbHasChildren.isChecked()) {
            cbHasChildrenVal = "1";
        } else {
            cbHasChildrenVal = "0";
        }
        if (cbMarried.isChecked()) {
            cbMarriedVal = "1";
        } else {
            cbMarriedVal = "0";
        }
        if (cbSmoke.isChecked()) {
            cbSmokeVal = "1";
        } else {
            cbSmokeVal = "0";
        }
        if (cbDrink.isChecked()) {
            cbDrinkVal = "1";
        } else {
            cbDrinkVal = "0";
        }

        view_liveWithIn_list();
        /*  action for taking spinner withIn value
         *  Created Date: 14-09-2020
         *  Updated Date:
         */

        spWithIn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spWithInID = spinWithIn_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*calling update method when click on button submit
         * Created Date: 14-09-2020
         */
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                update_setting();

            }
        });
    }
    /*  method for fetching live with in list
     *  Created Date: 14-09-2020
     */

    private void view_liveWithIn_list() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<WithInModel> call = serviceInterface.withIn_list(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<WithInModel>() {
            @Override
            public void onResponse(Call<WithInModel> call, Response<WithInModel> response) {
                if (response.isSuccessful()) {
                    WithInModel bean = response.body();
                    if (bean.getStatus().equals(true)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String heightName = bean.getData().get(i).getName();
                            Integer heightID = bean.getData().get(i).getId();

                            spinWithIn.add(heightName);
                            spinWithIn_id.add(heightID);

                        }
                    } else {
                        Toast.makeText(context, "something is wrong" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spWithIn.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinWithIn));
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WithInModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


    }

    /*  volley method for update_setting
     *  Created Date: 14-09-2020
     *  Updated Date:
     */
    private void update_setting() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("id", sharedPreference_main.getUserId());
            jsonBody.put("gender", GenderId);
            jsonBody.put("age_from", etAgeFrom.getText().toString());
            jsonBody.put("age_to", etAgeTo.getText().toString());
            jsonBody.put("live_within", spWithInID);
            jsonBody.put("has_children", cbHasChildrenVal);
            jsonBody.put("married", cbMarriedVal);
            jsonBody.put("smoke", cbSmokeVal);
            jsonBody.put("drink", cbDrinkVal);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + MAIL_PRIVACY, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                Intent i = new Intent(context, Setting.class);
                                startActivity(i);
                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
package com.nusys.datingapp.retrofit;


import com.nusys.datingapp.models.ActivityModel;
import com.nusys.datingapp.models.AllProfileFetchModel;
import com.nusys.datingapp.models.CityModel;
import com.nusys.datingapp.models.CountryModel;
import com.nusys.datingapp.models.FetchProfileModel;
import com.nusys.datingapp.models.ForgotPwdModel;
import com.nusys.datingapp.models.HeightModel;
import com.nusys.datingapp.models.InterestedInModel;
import com.nusys.datingapp.models.LikesModel;
import com.nusys.datingapp.models.LoginModel;
import com.nusys.datingapp.models.MessageModel;
import com.nusys.datingapp.models.ILikedModel;
import com.nusys.datingapp.models.OnlineModel;
import com.nusys.datingapp.models.PicsModel;
import com.nusys.datingapp.models.PlanDetailModel;
import com.nusys.datingapp.models.PlanModel;
import com.nusys.datingapp.models.ProfessionModel;
import com.nusys.datingapp.models.ProfileModel;
import com.nusys.datingapp.models.WhoViewedModel;
import com.nusys.datingapp.models.WithInModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

import static com.nusys.datingapp.commonModules.Constants.ACTIVITY;
import static com.nusys.datingapp.commonModules.Constants.BODY_TYPE;
import static com.nusys.datingapp.commonModules.Constants.CHILDREN;
import static com.nusys.datingapp.commonModules.Constants.CITY_LIST;
import static com.nusys.datingapp.commonModules.Constants.COUNTRY_LIST;
import static com.nusys.datingapp.commonModules.Constants.ETHNICITY;
import static com.nusys.datingapp.commonModules.Constants.FETCH_EDIT_VALUES;
import static com.nusys.datingapp.commonModules.Constants.FETCH_PROFILE;
import static com.nusys.datingapp.commonModules.Constants.FORGOT_PWD;
import static com.nusys.datingapp.commonModules.Constants.HEIGHT_LIST;
import static com.nusys.datingapp.commonModules.Constants.INTERESTED_IN;
import static com.nusys.datingapp.commonModules.Constants.INTERESTS;
import static com.nusys.datingapp.commonModules.Constants.I_LIKED_LIST;
import static com.nusys.datingapp.commonModules.Constants.LIKES_LIST;
import static com.nusys.datingapp.commonModules.Constants.LOGIN;
import static com.nusys.datingapp.commonModules.Constants.MARITAL_STATUS;
import static com.nusys.datingapp.commonModules.Constants.MEET;
import static com.nusys.datingapp.commonModules.Constants.MESSAGE_LIST;
import static com.nusys.datingapp.commonModules.Constants.ONLINE_LIST;
import static com.nusys.datingapp.commonModules.Constants.PICS_LIST;
import static com.nusys.datingapp.commonModules.Constants.PLANS;
import static com.nusys.datingapp.commonModules.Constants.PLANS_DETAIL;
import static com.nusys.datingapp.commonModules.Constants.PROFESSION_LIST;
import static com.nusys.datingapp.commonModules.Constants.RELIGION;
import static com.nusys.datingapp.commonModules.Constants.VIEWED_LIST;
import static com.nusys.datingapp.commonModules.Constants.WITH_IN_LIST;


/**
 * Created By: Shweta Agarwal
 * Created Date:01-07-2020
 * Updated Date:
 **/
public interface ServiceInterface {
    /**
     * login api POST API
     **/
    @POST(LOGIN)
    Call<LoginModel> doLogin(@Body HashMap<String, String> map);

    /**
     * view interested in list GET API
     **/
    @GET(INTERESTED_IN)
    Call<InterestedInModel> fetch_interested_in(@Header("Authorization") String token, @Header("Content-Type") String header);


    /**
     * view country list
     **/
    @GET(COUNTRY_LIST)
    Call<CountryModel> country_list(@Header("Authorization") String token, @Header("Content-Type") String header);

    //**view city list**//
    @POST(CITY_LIST)
    Call<CityModel> city_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    /**
     * view interested in list GET API
     **/
    @GET(BODY_TYPE)
    Call<InterestedInModel> fetch_body_type(@Header("Authorization") String token, @Header("Content-Type") String header);

    /**
     * view ethnicity list GET API
     **/
    @GET(ETHNICITY)
    Call<InterestedInModel> fetch_ethnicity(@Header("Authorization") String token, @Header("Content-Type") String header);

    /**
     * view marital status list GET API
     **/
    @GET(MARITAL_STATUS)
    Call<InterestedInModel> fetch_marital_status(@Header("Authorization") String token, @Header("Content-Type") String header);

    /**
     * view religion list GET API
     **//*
    @GET(RELIGION)
    Call<InterestedInModel> fetch_religion(@Header("Authorization") String token, @Header("Content-Type") String header);
*/
    /**
     * view children list GET API
     **/
    @GET(CHILDREN)
    Call<InterestedInModel> fetch_children(@Header("Authorization") String token, @Header("Content-Type") String header);

    /**
     * view interests list GET API
     **/
    @GET(INTERESTS)
    Call<InterestedInModel> fetch_interests(@Header("Authorization") String token, @Header("Content-Type") String header);

    //**view activity list**//
    @POST(ACTIVITY)
    Call<ActivityModel> activity_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**forgot password**//
    @POST(FORGOT_PWD)
    Call<ForgotPwdModel> forgot_pwd(@Body HashMap<String, String> map);

    //**view message list**//
    @POST(MESSAGE_LIST)
    Call<MessageModel> message_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**view message list**//
    @POST(LIKES_LIST)
    Call<LikesModel> likes_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**view message list**//
    @POST(ONLINE_LIST)
    Call<OnlineModel> online_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**view who viewed list**//
    @POST(VIEWED_LIST)
    Call<WhoViewedModel> who_viewed_me_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**view profile**//
    @POST(FETCH_PROFILE)
    Call<ProfileModel> fetch_profile(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    /**
     * view profession list
     **/
    @POST(PROFESSION_LIST)
    Call<ProfessionModel> profession_list(@Header("Authorization") String token, @Header("Content-Type") String header);

    /**
     * view height list
     **/
    @POST(HEIGHT_LIST)
    Call<HeightModel> height_list(@Header("Authorization") String token, @Header("Content-Type") String header);

    //**view plans**//
    @POST(PLANS)
    Call<PlanModel> fetch_plan(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    //**view plan detail**//
    @POST(PLANS_DETAIL)
    Call<PlanDetailModel> plan_detail(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);
    /**
     * view distance list
     **/
    @POST(WITH_IN_LIST)
    Call<WithInModel> withIn_list(@Header("Authorization") String token, @Header("Content-Type") String header);
    //**view pics list**//
    @POST(PICS_LIST)
    Call<PicsModel> pics_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);
    //**view i liked list**//
    @POST(I_LIKED_LIST)
    Call<ILikedModel> i_liked_list(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);
    //**fetch profile**//
    @POST(FETCH_EDIT_VALUES)
    Call<FetchProfileModel> fetch_editValues(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);

    @POST(RELIGION)
    Call<AllProfileFetchModel> fetch_religion(@Header("Authorization") String token, @Header("Content-Type") String header, @Body HashMap<String, String> map);
}

package com.nusys.datingapp.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.Activity;
import com.nusys.datingapp.activity.EditProfile;
import com.nusys.datingapp.activity.Home;
import com.nusys.datingapp.activity.Login;
import com.nusys.datingapp.activity.Premium;
import com.nusys.datingapp.activity.Setting;
import com.nusys.datingapp.commonModules.SharedPreference_main;

/**
 * Created By: Shweta Agarwal
 * Created DAte:27-08-2020
 * Updated Date:28-08-2020(apply intent on activities)
 * Updated Date: 04-08-2020(change activity to fragment)
 **/
public class MoreFragment extends Fragment {
   // Context context;
    SharedPreference_main sharedPreference_main;

   // LinearLayout llSetting, llLikes, llHome, llMsg;
    TextView tvEditProfile, tvSetting, tvActivity, tvLogout, tvPremium;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.activity_more, container, false);
        initialization(view);
        action();
        return view;
    }


    public void initialization(View view) {
       // context = MoreFragment.this;
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());
        //Hooks


       /* llSetting = view.findViewById(R.id.ll_setting);
        llLikes = view.findViewById(R.id.ll_likes);
        llHome = view.findViewById(R.id.ll_home);
        llMsg = view.findViewById(R.id.ll_msg);*/

        tvEditProfile = view.findViewById(R.id.tv_edit_profile);
        tvSetting = view.findViewById(R.id.tv_setting);
        tvActivity = view.findViewById(R.id.tv_activity);
        tvLogout = view.findViewById(R.id.tv_logout);
        tvPremium = view.findViewById(R.id.tv_premium);

    }

    public void action() {
        /**
         * intent in different layouts when click on different icons of bottom navigation
         * Created DAte:27-08-2020
         * Updated Date:
         **/
    /*    llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Home.class));
            }
        });
        llLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), LikesMe.class));
            }
        });

        llMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Message.class));
            }
        });
        llSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), MoreFragment.class));
            }
        });*/
        /**
         * add visibility and set alpha for bottom icons
         * Created DAte:27-08-2020
         * Updated Date:
         **/
     /*   llHome.setAlpha((float) 0.7);
        llSetting.setAlpha(1);*/
        // ivLikes.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.meet));

        /**
         * intent in edit profile when click on tvEditProfile
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditProfile.class));
            }
        });

        /**
         * intent in setting when click on tvSetting
         * Created DAte:28-08-2020
         * Updated Date:
         **/
        tvSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Setting.class));
            }
        });

        /**
         * intent in list of activity when click on tvActivity
         * Created DAte:31-08-2020
         * Updated Date:
         **/
        tvActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity.class));
            }
        });

        /**
         * intent on login when click on tvLogout by opening dialog box
         * Created DAte:31-08-2020
         * Updated Date:
         **/
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                // builder.setTitle("Confirm");
                builder.setMessage("Are you sure you want to logout?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        dialog.dismiss();
                        sharedPreference_main.removePreference();
                        sharedPreference_main.setIs_LoggedIn(false);
                        startActivity(new Intent(getContext(), Login.class));

                        getActivity().finish();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        /**
         * intent on premium when click on tvPremium
         * Created DAte:01-09-2020
         * Updated Date:
         **/
        tvPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Premium.class));
            }
        });

    }
}
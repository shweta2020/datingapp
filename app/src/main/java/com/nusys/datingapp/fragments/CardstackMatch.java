package com.nusys.datingapp.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.datingapp.R;
import com.nusys.datingapp.activity.Home;
import com.nusys.datingapp.adapter.CardsAdapter;
import com.nusys.datingapp.commonModules.SharedPreference_main;
import com.nusys.datingapp.models.CardItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import link.fls.swipestack.SwipeStack;

import static com.nusys.datingapp.R.layout.swipe_in_msg_view;

/**
 * Created By: Shweta Agarwal
 * Created DAte:13-08-2020
 * Updated Date:
 **/
public class CardstackMatch extends Fragment {
    SharedPreference_main sharedPreference_main;
    private SwipeStack cardStack;
    private CardsAdapter cardsAdapter;
    private ArrayList<CardItem> cardItems;
    private View btnCancel;
    private View btnLove;
    private int currentPosition;


    TextView tvMeetHeader, tvMatchHeader;
   // Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.activity_cardstack_match, container, false);
        initialization(view);
        action();
        return view;
    }

    public void initialization(View view) {
        cardStack = (SwipeStack) view.findViewById(R.id.container);
        btnCancel = view.findViewById(R.id.cancel);
        btnLove = view.findViewById(R.id.love);

        tvMeetHeader = view.findViewById(R.id.tv_meet_header);
        tvMatchHeader = view.findViewById(R.id.tv_match_header);

    }

    public void action() {
        //context = CardstackMatch.this;
        tvMeetHeader.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        tvMeetHeader.setTextColor(getResources().getColor(R.color.white));

        /* intent to home when click on tvMeetHeader
         * Created Date: 26-08-2020
         */

        tvMeetHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Home.class));
            }
        });


        setCardStackAdapter();
        currentPosition = 0;

        //Handling swipe event of Cards stack
        cardStack.setListener(new SwipeStack.SwipeStackListener() {
            @Override
            public void onViewSwipedToLeft(int position) {
               // cardStack.getOverlay().add(getDrawable(R.drawable.msg));
                currentPosition = position + 1;
            }

            @Override
            public void onViewSwipedToRight(int position) {
                currentPosition = position + 1;
            }

            @Override
            public void onStackEmpty() {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardStack.swipeTopViewToRight();

            }
        });

        btnLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "You liked " + cardItems.get(currentPosition).getName(),
                        Toast.LENGTH_SHORT).show();
                cardStack.swipeTopViewToLeft();
            }
        });
    }

    private void setCardStackAdapter() {
        cardItems = new ArrayList<>();

        cardItems.add(new CardItem(R.drawable.extrapic, "Huyen My", "Hanoi"));
        cardItems.add(new CardItem(R.drawable.lovelocation, "Do Ha", "Nghe An"));
        cardItems.add(new CardItem(R.drawable.extrapic, "Dong Nhi", "Hue"));
        cardItems.add(new CardItem(R.drawable.lovelocation, "Le Quyen", "Sai Gon"));
        cardItems.add(new CardItem(R.drawable.extrapic, "Phuong Linh", "Thanh Hoa"));
        cardItems.add(new CardItem(R.drawable.lovelocation, "Phuong Vy", "Hanoi"));
        cardItems.add(new CardItem(R.drawable.extrapic, "Ha Ho", "Da Nang"));

        cardsAdapter = new CardsAdapter(getActivity(), cardItems);
        cardStack.setAdapter(cardsAdapter);


    }



}